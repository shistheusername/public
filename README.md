### What is this repository for? ###

* This is my personal repo to backup and share various code and documents. You may freely use, modify, and redistribute the contents as long as you disclose where they are from.

### Contact ###

* email: <icanhasr00t@gmail.com>