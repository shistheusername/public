### What's in here ###

* This directory contains projects I wrote for fun and joy

### Index ###

1. 3colouring - Graph colouring problem with 3 colours, Java / C++
2. busDriver - Gossiping bus drivers from [here](https://www.reddit.com/r/dailyprogrammer/comments/4gqm90/20160427_challenge_264_intermediate_gossiping_bus/), C
3. founja - WIP, a game server for an online minesweeper style game, Java
4. simpleTFTP - TFTP([RFC 1350](https://tools.ietf.org/html/rfc1350)) server using kqueue for Mac, C
5. systemTB - JSON <-> [TLV](https://en.wikipedia.org/wiki/Type-length-value) encoder/decoder, Java
