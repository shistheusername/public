
import java.util.Hashtable;
import java.util.Iterator;
import java.util.PriorityQueue;

class Graph {

	private Hashtable<String, Vertex> vertices = new Hashtable<String, Vertex>();

	public void makeVertex(String name)
	{
		if(!vertices.containsKey(name))
			vertices.put(name, new Vertex(name));
	}

	public Vertex getVertex(String name)
	{
		return vertices.get(name);
	}

	public void makeEdge(String src, String dst)
	{
		// edge to self isn't allowed
		if(!src.equalsIgnoreCase(dst))
		{
			Vertex s = getVertex(src);
			Vertex d = getVertex(dst);
	
			s.createEdge(d);
			d.createEdge(s);
		}
	}

	public void printGraph()
	{
		Iterator<Vertex> itr = vertices.values().iterator();
		while(itr.hasNext())
			System.out.println(itr.next().toString());
	}

	public void run3colour()
	{
		// build a PQ with maximum property by # of adjacent nodes
		PriorityQueue<Vertex> pq = new PriorityQueue<Vertex>();
		pq.addAll(vertices.values());
		int loopCount = 0;
		while(!pq.isEmpty())
		{
			loopCount++;
			Vertex v = pq.remove();
			// always process vertex with the most adjacent nodes
			if(!colourVertex(v))
			{
				// Early termination is possible as we process a vertex with most possibility first
				System.out.println("Failed @ loop " + loopCount + "," 
								+ v.toString() + ", #nav:" + v.getNumOfNeighbours());
				System.exit(1);
			}
		}

		printGraph();
	}

	private boolean colourVertex(Vertex v)
	{
		// flags for not available colour
		int noR = Colour.NON.takenFlag();
		int noB = Colour.NON.takenFlag();
		int noG = Colour.NON.takenFlag();

		// check neighbours' colour
		Iterator<Vertex> itr = v.getNeighbours();
		while(itr.hasNext())
		{
			Vertex neighbour = itr.next();
			Colour ncolour = neighbour.getColour();

			// if this neighbour doesn't have a colour yet, move along
			if(ncolour == Colour.NON) continue; 

			// if it has any colour, set the flag for it
			if(noR != Colour.RED.takenFlag() && ncolour == Colour.RED) 
				noR = Colour.RED.takenFlag();
			else if(noB != Colour.BLUE.takenFlag() && ncolour == Colour.BLUE) 
				noB = Colour.BLUE.takenFlag();
			else if(noG != Colour.GREEN.takenFlag() && ncolour == Colour.GREEN) 
				noG = Colour.GREEN.takenFlag();
		}

		// choose next available colour in an order of R, B and G
		switch (noR+noB+noG)
		{
			case 0: // No colour around
			case 2: // No BLUE available
			case 4: // No GREEN available
			case 6: // Neither BLUE nor GREEN
				v.setColour(Colour.RED);
				break;
	
			case 1: // No RED available
			case 5: // Neither RED nor GREEN available
				v.setColour(Colour.BLUE);
				break;
	
			case 3: // no RED, no BLUE
				v.setColour(Colour.GREEN);
				break;
				
			default:
				return false; // Not possible
		}
		
		return true;
	}
}
