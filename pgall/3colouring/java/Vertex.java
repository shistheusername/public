
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

class Vertex implements Comparable<Vertex>{

	private String nm;
	private Colour colour;
	private Set<Vertex> adjacent; // Adjacent vertices

	public Vertex(String name) 
	{
		adjacent = new HashSet<Vertex>(); 
		colour = Colour.NON;
		nm = name;
	}

	public Iterator<Vertex> getNeighbours()
	{
		return adjacent.iterator();
	}
	
	public int getNumOfNeighbours()
	{
		return adjacent.size();
	}

	public Colour getColour()
	{
		return colour;
	}
	
	public void setColour(Colour c)
	{
		this.colour=c;
	}

	public void createEdge(Vertex neighbour)
	{
		adjacent.add(neighbour);
	}

	// Requirement for the Comparable interface
	public int compareTo(Vertex target) 
	{
		int mysize = adjacent.size();
		int nsize = target.getNumOfNeighbours();
		
		if (mysize > nsize) return -1;
		else if (mysize == nsize) return 0;
		else return 1;
	}

	public String toString()
	{
		return "V:"+nm+", Colour:"+colour.ordinal();
	}
}
