
import java.io.BufferedReader;
import java.io.FileReader;

public class Solution {

	static Graph g;

	public static void main(String[] args) 
	{
		if(args.length != 1)
		{
			System.out.println("Usage: java Solution datafile");
			System.exit(1);
		}else{
			g = new Graph();
			readData(args[0]);
			g.run3colour();
		}	
	}

	static void readData(String filename) {

		String line = null;
		boolean firstLine = true;

		try {

			BufferedReader reader = new BufferedReader(new FileReader(filename));
			while((line=reader.readLine()) != null)
			{
				if(firstLine)
				{
					g.makeVertex(line.trim());
					firstLine = false;
				}else{ // x <> y z 
					String[] data = line.split(" ");
					for(int i = 0; i < data.length; i++)
					{
						if(i==1) continue; // skip <>
						g.makeVertex(data[i].trim());
						if(i>1) g.makeEdge(data[0], data[i]);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
