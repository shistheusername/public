
public enum Colour {
	NON, RED, BLUE, GREEN;
	
	private int takenFlags[] = {0,1,2,4};
	
	public int takenFlag() {
		return takenFlags[this.ordinal()];
	}
}
