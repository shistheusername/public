#include <string>
#include <vector>
#include "Vertex.h"
using namespace std;
class Graph{
	friend ostream& operator<<(ostream&, const Graph&);
	int numOfNodes;
	vector<Vertex*> vertices;

	public: 
		bool operator()(Vertex*, Vertex*);
		void setNumOfNodes(int);
		void makeVertex(int);
		void makeEdge(int, int);
		void run3colour();
		bool colourVertex(Vertex*);
};
