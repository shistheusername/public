#ifndef _COLOUR_
#define _COLOUR_
enum Colour { NON, RED, BLUE, GREEN };
enum TakenFlags { NOFLAG=0, NORED=1, NOBLUE=2, NOGREEN=4 };
#endif
