#include <iostream>
#include <vector>
#include "Vertex.h"

using namespace std;
Vertex::Vertex(int name, int numOfNodes)
{
	this->name = name;
	colour = NON;
	numOfNeighbours = 0;
	adjacent.resize(numOfNodes);
}

int Vertex::getName()
{
	return name;
}

int Vertex::getNumOfNeighbours()
{
	return numOfNeighbours;
}

vector<Vertex*>& Vertex::getNeighbours ()
{
	return adjacent;
}

Colour Vertex::getColour ()
{
	return colour;
}

void Vertex::setColour (Colour c)
{
	colour = c;
}

void Vertex::createEdge (Vertex* n)
{	
	int idx = GETINDEX(n->getName());
	if(adjacent[idx] == nullptr) {
		adjacent[idx]=n;
		numOfNeighbours++;
	}
}

ostream& operator<<(ostream& os, const Vertex& v)
{
	os << "V:" << v.name << ", Colour:" << v.colour;
	return os;
}

ostream& operator<<(ostream& os, const Vertex* v)
{
	if(v!=NULL && v!=nullptr)
		os << "V:" << v->name << ", Colour:" << v->colour; // << ", AC:" << v->numOfNeighbours;

	return os;
}
