#include <iostream>
#include <vector>
#include <algorithm>
#include <cstddef>
#include "Graph.h"
#include "Vertex.h"
using namespace std;

void Graph::setNumOfNodes(int n)
{
	numOfNodes = n;
	vertices.resize(n);
}

void Graph::makeVertex(int name)
{
	int idx = GETINDEX(name);
	if(vertices[idx] == nullptr)
		vertices[idx] = new Vertex(name, numOfNodes);
}

void Graph::makeEdge(int src, int dst)
{
	if(src!=dst)
	{
		int sidx = GETINDEX(src);
		int didx = GETINDEX(dst);

		Vertex* s = vertices.at(sidx);
		Vertex* d = vertices.at(didx);

		s->createEdge(d);
		d->createEdge(s);
	}
}

void Graph::run3colour()
{
	vertices.shrink_to_fit();
	sort(vertices.begin(), vertices.end(), *this);

	for(auto const& v: vertices)
	{
		if(v==nullptr) continue;

		if(!colourVertex(v))
		{
			cout << "Impossible" << endl;
			exit(1);
		}
	}
}

bool Graph::colourVertex(Vertex* v)
{
	int noR = NOFLAG;
	int noB = NOFLAG;
	int noG = NOFLAG;

	vector<Vertex*> &neighbours = v->getNeighbours();
	neighbours.shrink_to_fit();

	// check neighbours' colour
	for(auto const& u: neighbours)
	{
		if(u!=nullptr)
		{
			Colour ncolour = u->getColour();
		
			if(ncolour == NON) continue;

			if(noR != NORED && ncolour == RED)
				noR = NORED;
			else if(noB != NOBLUE && ncolour == BLUE)
				noB = NOBLUE;
			else if(noG != NOGREEN && ncolour == GREEN)
				noG = NOGREEN;

		}
	}

	// pick available colour
	switch(noR+noB+noG)
	{
		case 0:
		case 2:
		case 4:
		case 6:
			v->setColour(RED);
			break;
		case 1:
		case 5:
			v->setColour(BLUE);
			break;

		case 3:
			v->setColour(GREEN);
			break;
		default:
			return false;
	}

	return true;
}

bool Graph::operator() (Vertex* s, Vertex* d) 
{
	if(s == d) return false;
	if(s == NULL) return false;
	if(d == NULL) return false;
	return (s->getNumOfNeighbours() > d->getNumOfNeighbours());
}

ostream& operator<<(ostream& os, const Graph& g)
{ 
	for (auto const& v : g.vertices)
	{
		if(v!=nullptr)
			os << v << endl;
	}
	return os;
}
