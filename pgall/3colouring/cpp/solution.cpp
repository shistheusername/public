#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <stdexcept>
#include "Graph.h"

using namespace std;

void readData(const char*, Graph*);
void error(const char*, const char*);

int main(int argc, char** argv)
{
	if(argc!=2)
	{
		cout<< "Usage: " << argv[0] << " datafile" << endl;
		exit(1);
	}else{
		Graph g;
		readData(argv[1], &g);
		g.run3colour();
		cout << g << endl;
	}
	return 0;
}

void error(const char* msg, const char* obj="")
{
	cerr << msg << ' ' << obj << '\n';
	exit(1);
}

void readData(const char* filename, Graph* g)
{
	try {

		ifstream inputFile(filename, ifstream::in);
		if(!inputFile) error("Failed to open file", filename);  

		string line;
		bool firstLine = true;

		while(getline(inputFile,line))
		{ 
			istringstream ist(line);
			string word;
			int numNode, thisNode = 0;

			if(firstLine)
			{

				ist>>word;
				numNode = stoi(word);
				g->setNumOfNodes(numNode);
				g->makeVertex(numNode);
				firstLine = false;

			}else{

				while(ist>>word) {
					if(word == "<>") continue; // skip <>
					numNode = stoi(word);
					g->makeVertex(numNode);
					if(!thisNode) thisNode = numNode;
					g->makeEdge(thisNode, numNode);
				}

			}
		}
	}catch(const exception &e){
		cerr << __FUNCTION__ << " exception on " << e.what() << endl;
	}catch(...){
		error("WTF");
	}
}
