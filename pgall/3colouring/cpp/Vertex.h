#include <vector>
#include "Colour.h"

#ifndef _VERTEX_
#define _VERTEX_

#define GETINDEX(X) ((X<=0) ? 0 : (X-1))
using namespace std;

class Vertex {
	friend ostream& operator<<(ostream&, const Vertex&);
	friend ostream& operator<<(ostream&, const Vertex*);

	int name;
	Colour colour;
	vector<Vertex*> adjacent;
	int numOfNeighbours;

	public:
		Vertex (int name, int numOfNodes);
		int getName();
		vector<Vertex*>& getNeighbours();
		int getNumOfNeighbours();
		Colour getColour ();
		void setColour (Colour c);
		void createEdge (Vertex* neighbour);
};
#endif
