/*
 * This file contains definitions of data structures and function prototypes
 * regarding the features related to the configuration file.
 */
#include <string.h>
#include "constants.h"
#include "util.h"

/**
 * Data structure for server's global configuration 
 *
 * This data structure will hold all variables that can be configurable via
 * a configuration file.
 */
typedef struct _config {
	int port;					// service port 
	int max_threads;			// Maximum number of active threads
	int max_clients;			// Maximum number that a service thread can serve
	int start_thread_count;		// Minimum number of threads in the thread pool
	int idle_timeout;			// Idle timeout for service threads
	int timeout;				// timeout for server socket
	int stacksize;				// Stack size for each worker thread
	int stat_interval;			// Seconds that the gc will sleep

	int min_idle_threads;
	int max_idle_threads;

	char pidfile[MAX_STRING_LEN];	// This file will store server process' PID
	char tftp_boot[MAX_STRING_LEN];	// Server's main document root
	char runas[MAX_STRING_LEN];	// A user name that the server will run as

} config_rec;

/**
 * Data structure for the keywords that server will recognize in the configuration file
 *
 * This structure is used to validate configuration kewords more effectively.
 * you will see more details in the http_config.c
 */
typedef struct _directives {
	char *name;					// keyword name
	short type;					// data type of the keyword in the config_rec
	void *value;				// value read from the configuration file

} DIRECTIVE;

// function prototypes
void config_error(char *error_msg, char *filename, int lineno,
				  FILE * errors);
void setup_default_configuration();
void read_configuration(const char *, FILE *);
int find_directive(char *);
void print_configuration(config_rec *);
