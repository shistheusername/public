#include <string.h>
#include <ctype.h>
#include "constants.h"
#include "util.h"

/*
 * Need some lowercase words?
 * 
 * This function changes the case of given string into lowercase.
 * str is a pointer to the word that will be lowercased(?) 
 */
void str2lower(char *str)
{
	int i;						// anonymous helper

	// assert the input first
	if (str == NULL || strlen(str) <= 0) {
		fprintf(stderr, "NULL parameter into str2lower.\n");
		return;
	}
	// super simple loop
	for (i = 0; str[i] != '\0'; i++) {
		if (isupper(str[i]))	// is it a big one?
		{
			str[i] = tolower(str[i]);	// make it small!
		}
	}
}

/**
 * No comment is allowed.
 * 
 * This function will read a line from the file pointed by fp into the linebuf.
 * All lines started with # will be ignored.
 *
 * linebuf is a char pointer to the buffer that we will fill up
 * maxlength is the maximum length of a line
 * fp is a FILE pointer to the file we will read from
 * lineno is an integer pointer keeping # of lines we have processed
 */
int getline(char *linebuf, int maxlength, FILE * fp, int *lineno)
{
	// helper variables, they are always friendly
	// even though, no one cares about them ;^)
	int len = 0;
	char c;

	// empty the line buffer
	bzero(linebuf, maxlength);

	// skip leading whitespaces
	do {
		c = fgetc(fp);
	}
	while (isspace(c));

	// ignore whatever after # and an empty line, return 0 of length
	if (c == '#' || c == EOL) {
		// read until we get to the end of the line
		while ((c = fgetc(fp)) != EOL);

		// we are done!
		(*lineno)++;
		return len;
	}
	// if we are here, then it means that we have met a line 
	// that we should care

	// we passed over 1 character, so go back
	ungetc(c, fp);

	// read actual characters for directives
	while ((c = fgetc(fp)) != EOL && c != EOF) {
		// if this line is too long, read upto the length of the maxlength
		if (len >= maxlength) {
			linebuf[len - 1] = '\0';
			return len;
		}
		// There should be only one white space between a directive and its value
		if (isspace(c)) {
			linebuf[len++] = ' ';	// the space

			// skip over any other spaces
			while (isspace(c))
				c = fgetc(fp);
		}
		// load the character into the buffer
		linebuf[len++] = c;
	}

	// are we at the end of line? yet?
	if (c == EOL) {	// blast trailing whitepaces 
		while (len > 0 && isspace(linebuf[len - 1]))
			--len;

		// finish the line
		linebuf[len] = '\0';
		(*lineno)++;

		return len;
	}
	// we got the EOF
	return FAIL;
}
