/**
 * This file contains implementation of functions related to socket operations
 */
#include "socket.h"
#include "configuration.h"
#include "tftp_request.h"
#include "file.h"
#include <signal.h>
#include <sched.h>
#include <pthread.h>

extern config_rec configuration;

ERRMSG errMsgs[] = {
	{EUNDEF, "Undefined error code"},
	{ENOTFOUND, "File not found"},
	{EACCESS, "Access violation"},
	{ENOSPACE, "Disk full or allocation exceeded"},
	{EBADOP, "Illegal TFTP operation"},
	{EBADID, "Unknown transfer ID"},
	{EEXISTS, "File already exists"},
	{ENOUSER, "No such user"},
};
int chKqEvent(int kq, int sock, int filter, int action, void* udata)
{
	struct kevent ev;
	EV_SET(&ev, sock, filter, action, 0, 0, udata);
	if (FAIL == kevent(kq, &ev, 1, NULL, 0, NULL))
	{
		syslog(LOG_ERR, "chKqEvent:kevent failed; %s", strerror(errno));
		return FAIL;
	}

	return TRUE;
}

int rmKqEvent(int kq, int sock, int filter)
{
	struct kevent ev;
	EV_SET(&ev, sock, filter, EV_DELETE, 0, 0, NULL);
	if (FAIL == kevent(kq, &ev, 1, NULL, 0, NULL))
	{
		syslog(LOG_ERR, "rmdKqEvent:kevent failed; %s", strerror(errno));
		return FAIL;
	}

	return TRUE;
}

int addKqEvent(int kq, int sock, int filter, void* udata)
{
	struct kevent ev;
	if(kq == FALSE) 
	{
		kq = kqueue();
		if (kq == FAIL) return kq;
	}
	
	EV_SET(&ev, sock, filter, EV_CLEAR|EV_ADD, 0, 0, udata);
	if (FAIL == kevent(kq, &ev, 1, NULL, 0, NULL))
	{
		syslog(LOG_ERR, "addKqEvent:kevent failed, %m");
		return FAIL;
	}

	return kq;
}

int create_bind_socket(int port)
{
	int sock;
	struct sockaddr_in sa;

	// create
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock == FAIL) return FAIL;
	if (fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) | O_NONBLOCK) == FAIL) 
	{
		close(sock);
		return FAIL;
	}

	// bind
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;
	sa.sin_port = htons(port);
	/*
	if (reusable)
		setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reusable, sizeof reusable);
	*/

	if(FAIL == bind(sock, (struct sockaddr *) &sa, sizeof(sa)))
	{
		close(sock);
		return FAIL;
	}

	return sock;
}

/**
 * Send an error message to a client
 *
 * @param sock a descriptor of socket we will use
 * @param to a pointer to a client's sockaddr_in
 * @param error an error code
 */
void nak(request* req, int error)
{
	char buf[SEGSIZE] = { 0 };
	register struct tftphdr *hdr;
	register int len;
	register struct errmsg *pe;

	// Generate an error header
	hdr = (struct tftphdr *) buf;
	hdr->th_opcode = htons((u_short) ERROR);
	hdr->th_code = htons((u_short) error);
	
	// Find appropriate error message
	for (pe = errMsgs; pe->e_code >= 0; pe++)
		if (pe->e_code == error)
			break;
	
	// Setup the error code
	if (pe->e_code < 0)
		pe->e_msg = strerror(error);

	len = strlen(pe->e_msg);
	strncpy(hdr->th_msg, pe->e_msg, len);
	hdr->th_msg[len] = '\0';

	// add the length of meta data in the header
	len += OPSIZE+BLOKSIZE+1;

	// Shooot
	if(FAIL == sendData(req, buf, len))
	{
		syslog(LOG_ERR, "Failed NACK: %m");
		return;
	}
}

void ack(request* req)
{
	char buf[8] = { 0 };
	register struct tftphdr *hdr;
	register int len;

	// Generate an ACK header
	hdr = (struct tftphdr *) buf;
	hdr->th_opcode = htons((u_short) ACK);
	hdr->th_code = htons((u_short) req->blknum);
	
	len = OPSIZE+BLOKSIZE;

	if(FAIL == sendData(req, buf, len))
	{
		syslog(LOG_ERR, "Failed ACK: %m");
		return;
	}
}

int recvData(request* req, char* buf)
{
	errno = 0;
	int len = recv(req->evds, buf, MAXPKTSIZE, 0);
	if (len < 0) {
		syslog(LOG_ERR, "Error on recv from %s:%d: %m", inet_ntoa(req->client.sin_addr), ntohs(req->client.sin_port));
		return FAIL;
	}
	return len;
}

int sendData(request* req, char* buf, int len)
{
	errno = 0;
	if (send(req->evds, (void *) buf, len, 0) != len) 
	{
		syslog(LOG_ERR, "sendData, ERROR, send to client(%s:%d): %m\n", 
							inet_ntoa(req->client.sin_addr), ntohs(req->client.sin_port));
		return FAIL;
	}
	return TRUE;
}

