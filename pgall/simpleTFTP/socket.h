#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/tftp.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <syslog.h>
#include <errno.h>
#include <sys/param.h>
#include <sys/socket.h>
#include "tftp_request.h"

#ifdef USE_EPOLL
#include <sys/epoll.h>
#endif

#ifdef USE_KQ
#include <sys/event.h>
#endif

typedef struct errmsg {
	int e_code;
	const char *e_msg;
} ERRMSG;

#define ACK_MISMATCH_NOLOG 400
#define ACK_MISMATCH_LOG 401
#define UNEXPECTED_OPCODE 402

int create_bind_socket(int port);

#ifdef USE_EPOLL
int addEpollEvent(int sock, short et);
#endif

#ifdef USE_KQ
int addKqEvent(int kq, int sock, int filter, void* udata);
int rmKqEvent(int kq, int sock, int filter);
int chKqEvent(int kq, int sock, int filter, int action, void* udata);
#endif

int sendData(request*, char*, int);
int recvData(request*, char*);
void nak(request* req, int);
void ack(request* req);
