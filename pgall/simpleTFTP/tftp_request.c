#include <pthread.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include "file.h"
#include "tftp_request.h"
#include "socket.h"


// little function for debugging
void printRequest(request * req)
{
	fprintf(stdout, "REQUEST\n");
	fprintf(stdout, "==========================\n");
	fprintf(stdout, "State : %d\n", req->state);
	fprintf(stdout, "EVDS : %d\n", req->evds);
	fprintf(stdout, "TYPE : %d\n", req->reqType);
	fprintf(stdout, "FILENAME : %s\n", req->filename);
	fprintf(stdout, "MODE : %c\n", req->mode);
	fprintf(stdout, "BLKSIZE : %u\n", req->blksize);
	fprintf(stdout, "BLKNUM : %u\n", req->blknum);
	fprintf(stdout, "LASTBLK : %lld\n", req->lastblock);
	fprintf(stdout, "CHUNKS : %u\n", req->chunks);
	fprintf(stdout, "TIMEOUT : %d\n", req->timeout);
	fprintf(stdout, "TSIZE : %c\n", req->tsize);
	fprintf(stdout, "==========================\n");
}


/**
 * process WRQ request packet
 *
 * @param req request pointer
 * @return 
 * 		FAIL - should end this request
 * 		TRUE - OACK sent 
 */
int processWRQ(request* req)
{
	/* 
	 * TODO:This check should be toggled by a configuration value
	 *
	if(FAIL!=verifyFile(req->filename))
	{
		nak(req, EEXISTS);
		return FAIL;
	}
	*/

	// file too big
	if(req->tsize_value > INT_MAX)
	{
		nak(req, ENOSPACE);
		return FAIL;
	}
	
	// Open the requested file
	req->fd = open(req->filename, O_WRONLY|O_TRUNC|O_CREAT|O_NONBLOCK, 0644);
	if (req->fd == FAIL) {
		syslog(LOG_ERR, "Error while opening the file: %s, %m\n", req->filename);
		nak(req, errno);
		return FAIL;
	}

	req->blknum = 0;
	req->state = RECV_DATA;

	return TRUE;
}


/**
 * Serve a RRQ request
 *
 * @param req a pointer to the request we are serving
 * @return TRUE or FALSE
*/
int processRRQ(request* req)
{
	syslog(LOG_ERR, "processRRQ: sock = %d\n", req->evds);
	
	// verifyFile first
	req->size = verifyFile(req->filename);
	if (req->size == FAIL) {
		syslog(LOG_ERR, "Error on verifyFile:%s, %m\n", req->filename);
		nak(req, errno);
		return FAIL;
	}
	
	// Open the requested file
	req->fd = open(req->filename, O_RDONLY);
	if (req->fd == FAIL) {
		syslog(LOG_ERR, "Error while opening the file: %s, %m\n", req->filename);
		nak(req, errno);
		return FAIL;
	}

	req->blknum = 1;

	// How many chunks should we send?
	req->chunks = req->size / req->blksize;
	req->lastblock = req->size % req->blksize;
	if (req->lastblock > 0) req->chunks += 1;

	printRequest(req);

	syslog(LOG_ERR, "file %s(%ld bytes), blksize:%ld, blknum:%ld, chunks:%ld ", 
					req->filename, req->size, req->blksize, req->blknum, req->chunks);

	req->state = SEND_DATA;
	return TRUE;
}

int sendBlock(request* req)
{
	unsigned int len;
	char sendbuf[MAXPKTSIZE] = { 0 };
	register struct tftphdr *hdr = (struct tftphdr *) sendbuf;

	errno = 0;
	if(req->fileAddr == NULL)
	{
		// Mmap the file, TODO: use sendfile...ascii mode???
		req->fileAddr = (char *) mmap((caddr_t) 0, req->size, PROT_READ, MAP_PRIVATE, req->fd, 0);
		if (req->fileAddr == MAP_FAILED) {
			syslog(LOG_ERR, "Error while mmaping the file: %s, %m\n", req->filename);
			nak(req, errno);
			goto CLEANUP;
		}
		req->readPtr = req->fileAddr;
	}
			
	len = (req->chunks > 1) ? req->blksize : req->lastblock;

	//if (req->mode == 'o') {
	bcopy(req->readPtr, hdr->th_msg, len);
	req->readPtr += len;

	// send data and recv an ACK
	hdr->th_opcode = htons(DATA);
	hdr->th_block = htons(req->blknum);
	req->server_opcode = DATA;

	syslog(LOG_ERR, "sendDATA: Len:%d, BLKNUM:%d, chunks:%d\n", len, req->blknum, req->chunks);
	if(FAIL == sendData(req, sendbuf, len+OPSIZE+BLOKSIZE))
	{
		syslog(LOG_ERR, "sendData failed to send %s to %s:%d\n", req->filename, inet_ntoa(req->client.sin_addr),
				   ntohs(req->client.sin_port));
		goto CLEANUP;
	}

	req->state = RECV_ACK;
	return TRUE;

CLEANUP:
	close(req->fd);
	if (req->size > 0 && munmap(req->fileAddr, req->size) == FAIL)
		syslog(LOG_ERR, "Warning: failed to munmap:%m");

	if (errno == 0) {

		syslog(LOG_INFO, "%s FINISHED, %d block served for %s:%d",
			   req->filename, (req->blknum - 1),
			   inet_ntoa(req->client.sin_addr),
			   ntohs(req->client.sin_port));

		return TRUE;

	} else {

		syslog(LOG_ERR,
			   "%s FINISHED WITH ERRORS, client:%s:%d",
			   req->filename,
			   inet_ntoa(req->client.sin_addr),
			   ntohs(req->client.sin_port));

		return FAIL;
	}
}

/**
 * Parse the request came in and run some error checking
 *
 * @param req a pointer to the request that worker threads will access
 * @param data a pointer to the actual data read from client
 * @param reqLen the length of the data
 *
 * @return TRUE or FAIL
 */
int parseFirstRequest(request* req, char* data, int reqLen)
{
	register struct tftphdr *hdr = (struct tftphdr*) data;
	register char *s;
	register int i;

	errno = 0;
	hdr->th_opcode = ntohs(hdr->th_opcode);
	if (hdr->th_opcode == ACK)
		return FAIL;

	syslog(LOG_INFO, "Parsing the request....");
	bzero(req, sizeof(request));

	// initiate the request with default values
	req->evds = -1;
	req->blksize = SEGSIZE;
	req->timeout = DEFAULT_TIMEOUT;
	req->reqType = hdr->th_opcode;
	req->tsize = 'f';

	// points to beginning of filename
	s = hdr->th_stuff;
	reqLen -= sizeof(short);

	// Check filename
	if (strstr(s, "/../") || !strncmp(s, "..", 2)) 
	{
		// free(req); 
		syslog(LOG_ERR, "Error: invalid access:%s\n", s);
		errno = EACCESS;
		return FAIL;
	}

	// Filename
	if (*s == '/') s++;
	strncpy(req->filename, s, strlen(s) + 1);
	readAhead(s, reqLen);

	// Mode 
	if (strcasecmp("netascii", s) == 0)
		req->mode = 'a';
	else if (strcasecmp("octet", s) == 0)
		req->mode = 'o';
	else {
		syslog(LOG_ERR, "Error: unknown transfer mode:%s\n", s);
		errno = EBADOP;
		return FAIL;
	}
	readAhead(s, reqLen);

	// Read in options
	while (0 < reqLen) {
		if (!strcasecmp("tsize", s)) {
			req->tsize = 't';
			readAhead(s, reqLen);

			req->tsize_value = atoi(s);
			readAhead(s, reqLen); // useful for only WRQ
		}

		if (!strcasecmp("timeout", s)) {
			readAhead(s, reqLen);
			req->timeout = atoi(s);
			if (req->timeout < MINTIMEOUT)
				req->timeout = MINTIMEOUT;
			else if (req->timeout > MAXTIMEOUT)
				req->timeout = MAXTIMEOUT;
			readAhead(s, reqLen);
		}

		if (!strcasecmp("blksize", s)) {
			readAhead(s, reqLen);
			req->blksize = (unsigned int) atol(s);
			if (req->blksize < SEGSIZE)
				req->blksize = SEGSIZE;
			else if (req->blksize > MAXSEGSIZE)
				req->blksize = MAXSEGSIZE;
			readAhead(s, reqLen);
		}
	}

	return TRUE;
}

/**
 * Receive a tftp packet 
 * 
 * @param req request pointer
 * @return a block number received or FAIL
 */
int recvBlock(request* req)
{
	char buf[MAXPKTSIZE] = { 0 };
	char ascBuf[MAXPKTSIZE] = { 0 };

	struct tftphdr *hdr = (struct tftphdr *) buf;

	int len;
	int sock = req->evds;

	len = recvData(req, buf);
	if(len == FAIL)
	{
		syslog(LOG_ERR, "recvData failed, %m");
		return FAIL;
	}

	syslog(LOG_DEBUG, "recvData read %d bytes, blksize: %d from %s:%d via sock: %d", 
			len, req->blksize, inet_ntoa(req->client.sin_addr), ntohs(req->client.sin_port), sock); 

	if(req->state == RECV_DATA)
	{
		len -= (OPSIZE+BLOKSIZE);
		if(req->mode == 'a'){
			len = convertAsc(hdr->th_msg, ascBuf, len);
			if(len != FAIL)
				len = write(req->fd, ascBuf, MIN(len, req->blksize));
			else errno = EBADOP;
		} else {
			len = write(req->fd, hdr->th_msg, MIN(len, req->blksize));
		}

		if(FAIL == len)
		{
			syslog(LOG_ERR, "Failed to write into file: %s, %m", req->filename);
			close(req->fd);
			return len;
		}
		syslog(LOG_ERR, "Write wrote: %d bytes", len);
		// last, close the file
		if(len < req->blksize) close(req->fd);

		//short opcode = ntohs(hdr->th_opcode);
		req->blknum = ntohs(hdr->th_block);
		return TRUE;
	}

	if(req->state == RECV_ACK)
	{
		errno = 0;
		len = parseACK(req, hdr);
		if (errno)	// Error occurred
		{
			switch(errno)
			{
				case ACK_MISMATCH_LOG: 
					if (req->retry == 0)	// No more retry, can't fix the error
					{
						syslog(LOG_ERR,
							"Error, too many consecutive ACK mismatch occured transferring %s -> %s:%d, expected: %d, received: %d",
							req->filename,
							inet_ntoa(req->client.sin_addr),
							ntohs(req->client.sin_port),
							req->blknum, ntohs(hdr->th_block));

						nak(req, EBADID);	// Non-recoverable Unknown transfer id
					}
				case ACK_MISMATCH_NOLOG:
					syslog(LOG_ERR, "RECV_ACK for ACK_MISMATCH_NOLOG");
					errno = EBADID;
					return FAIL;
						
				case UNEXPECTED_OPCODE:
					syslog(LOG_ERR,
						"Error, received an unexpected opcode(%d) from client,%s:%d\n",
						ntohs(hdr->th_opcode), inet_ntoa(req->client.sin_addr), ntohs(req->client.sin_port));
						nak(req, EBADOP);	// Illegal TFTP operation
					return FAIL;
			}
		}
		return len;
	}
	return TRUE;
}

/** 
 * Create an OACK if there is any custom option
 *
 * @param ptr a pointer to a buffer we will fill
 * @param req a pointer to the request we are serving
 *
 * @return SEGSIZE - sizeof(OACK)
 */
int createOACK(char *ptr, void *Req)
{
	register int sent, left = DATASIZE;
	request *req = (request *) Req;

	// block size
	if (req->blksize > SEGSIZE) {
		syslog(LOG_ERR,
			   "OACK, client(%s:%d) wants to setup blksize to %d bytes.",
			   inet_ntoa(req->client.sin_addr),
			   ntohs(req->client.sin_port), req->blksize);

		sent = snprintf(ptr, left, "%s", "blksize");
		fwdPtr(ptr, left, sent);

		sent = snprintf(ptr, left, "%d", req->blksize);
		fwdPtr(ptr, left, sent);
	}

	// Check the size of requested file 
	if (req->tsize == 't') {
		syslog(LOG_INFO,
			   "OACK, client(%s:%d) wants to use tsize option.",
			   inet_ntoa(req->client.sin_addr),
			   ntohs(req->client.sin_port));

		sent = snprintf(ptr, left, "%s", "tsize");
		fwdPtr(ptr, left, sent);

		sent = snprintf(ptr, left, OFF_T_FMT, verifyFile(req->filename));
		fwdPtr(ptr, left, sent);
	}

	return left;
}

/**
 * Parse ACK data from a client and make it sure everything is fine 
 *
 * @param server_send_buffer a pointer to server's send buffer
 * @param client_response_buffer a pointer to client's response buffer
 *
 * @return TRUE for OACK, a block number for DATA, or FAIL on Error
 */
int parseACK(request* req, struct tftphdr *client_response_buffer)
{
	short client_opcode = ntohs(client_response_buffer->th_opcode);
	unsigned short client_block = ntohs(client_response_buffer->th_block);

	errno = 0;

	syslog(LOG_ERR, "parseACK:s_opcode; %d, s_blknum: %d, c_opcode: %d, c_blknum: %d", req->server_opcode, req->blknum, client_opcode, client_block);
	// check if opcode is an ACK and return hdr->th_block!
	switch (client_opcode) {
	case ACK:
		// For an OACK, all we needed was an ACK
		if (req->server_opcode == OACK)
			return TRUE;

		// the client_opcode is a response for a DATA block server have sent
		if (req->server_opcode == DATA) {

			if (req->blknum == client_block)
				return client_block;

			// Needed to be more resilient to network problems
			errno = (1 != abs(req->blknum - client_block)) ? ACK_MISMATCH_LOG : ACK_MISMATCH_NOLOG;
		}
		break;

	case ERROR:	// Client reported an error on his side
		errno = ntohs(client_response_buffer->th_code);
		break;

	default:	// Unexpected code, something is wrong
		errno = UNEXPECTED_OPCODE;
	}

	return FAIL;
}

int sendOACK(request* req, int nextState)
{
	int len;
	char sendbuf[PKTSIZE] = { 0 };
	register struct tftphdr *hdr = (struct tftphdr *) sendbuf;

	// Create an OACK if custom options requested
	if ((len = createOACK(hdr->th_stuff, req)) < DATASIZE) 
	{
		hdr->th_opcode = htons(OACK);
		len = SEGSIZE - len;

		// No retry for OACK, just start sending file, clients don't follow RFC2349 strictly
		if(FAIL!=sendData(req, sendbuf, len))
		{
			req->state = nextState;
			req->server_opcode = OACK;
			req->blknum = 0;
			return TRUE;
		}
	}
	return FALSE; // not fail as we can still function
}
