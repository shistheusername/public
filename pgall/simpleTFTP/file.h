#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "constants.h"

off_t verifyFile(char *filename);
unsigned int convertAsc(char *src, char *dst, unsigned int len);
