#include <netinet/in.h>
#include <arpa/tftp.h>
#include <limits.h>
#include "constants.h"

#ifndef TFTP_REQ
#define TFTP_REQ

#define OACK 06
#define fwdPtr(ptr, left, len) if(len++ > 0) { ptr += len; left -= len; }
#define RETRY 3

#define OPSIZE  sizeof(short)
#define BLOKSIZE  sizeof(unsigned short)
#define DATASIZE  (SEGSIZE - OPSIZE)

#if _FILE_OFFSET_BITS == 64
#define	OFF_T_FMT	"%lld"
#else
#define	OFF_T_FMT	"%ld"
#endif

enum state { SEND_DATA, RECV_ACK, SEND_ACK, RECV_DATA };

// data structure representing a client's request
typedef struct _req {

	struct sockaddr_in client;	// client address, which will be set by main thread()
	short reqType;				// RRQ, WRQ?
	char filename[FILENAME];	// What the client wants
	char mode;					// a - ascii, o - octet
	int evds;					// epoll fd
	int timeout;				// timeout

	// extended tftp options
	unsigned int blksize;		// block size, default 512
	char tsize;					// tsize, t - true, f - false
	int tsize_value;			// UPTO INT_MAX

	// file transfer
	int state, fd;
	unsigned int chunks;
	off_t size, lastblock;
	char *fileAddr, *readPtr;
	unsigned short blknum;
	short server_opcode; 
	short retry; // TODO: implement

} request;

#define readAhead(ptr, len) i = strlen(s) + 1; s += i; reqLen -= i;

void printRequest(request*);
int processRRQ(request*);
int processWRQ(request*);
int sendBlock(request*);
int recvBlock(request*);
int parseFirstRequest(request*, char*, int);
int parseACK(request*, struct tftphdr*);
int createOACK(char*, void*);
int sendOACK(request*, int);

#endif
