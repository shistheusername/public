#include <sys/resource.h>
#include <sys/stat.h>
#include <pwd.h>
#include <signal.h>
#include "configuration.h"
#include "socket.h"

void cleanUp(int, request*, int);
void chuser();
void max_resource();
void log_pid(char *);
void daemonize();
