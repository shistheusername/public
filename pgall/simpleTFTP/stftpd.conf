#===========================================================================
# Simple TFTP Daemon(STFTPD) configuration ile
#===========================================================================

# Port: port number to which STFTPD will listen. 
# For ports < 1023, you need to be privileged
# Default: 69 (or DEFAULT_PORT)
Port 69

# RunAs: name of a system user STFTPD will run as.
# Default: root(or DEFAULT_USER)
# RunAs root

# StatInterval: time period in seconds which statistics information will be collected
# Set this to 0 in order to disable the feature
# Default: 10(or DEFAULT_STAT_INTERVAL)
StatInterval 10

# MinIdleThreads: number of idle service threads before new threads get created
# Default: 5 (or DEFAULT_MIN_IDLE_THREADS)
MinIdleThreads 5

# MaxIdleThreads: number of idle service threads before they get killed
# Default: 10 (or DEFAULT_MAX_IDLE_THREADS)
MaxIdleThreads 10

# StartThreads: number of service threads to launch on start-up
# Default: 5 (or DEFAULT_START_THREAD)
StartThreads 5

# MaxThreads: number of service threads that can run concurrently
# Default: 10000 (or DEFAULT_MAX_THREAD)
MaxThreads 100

# TimeOut: number of seconds a service thread will wait for a client to send its request once connected, or 
# the maximum amount of time the server will spend waiting for a client to receive data.
# Default: 5 (or DEFAULT_TIMEOUT)
TimeOut 5

# IdleTimeOut: number of seconds a service thread will wait for a new request before commiting suicide
# Default: 5 (or DEFAULT_IDLE_TIMEOUT)
IdleTimeOut 30

# MaxClients: number of clients a service thread will serve before commiting a suicide
# Default: 5 (or DEFAULT_MAX_CLIENTS)
MaxClients 20

# StackSize: maximum size of the process stack, in Kbytes. 
# DON'T TOUCH THIS unless you know what you are doing. Set to 0 to use the system default value, which is 10MB.
# The higher you set, the less # of threads you will get.
# Default: 128 (or DEFAULT_STACKSIZE)
StackSize 4096

# PidFile: the file where STFTPD records its process ID (PID)
# Default: /var/run/stftpd.pid (or DEFAULT_PIDFILE)
# PidFile stftp.pid

# TftpBoot: absolute path of the directory containing files to be served
# Default: /tftpboot (or DEFAULT_TFTPBOOT)
TftpBoot ./tftpboot
