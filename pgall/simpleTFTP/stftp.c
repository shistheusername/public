#include "stftp.h"

// stftp.conf stuff
extern config_rec configuration;
static unsigned int num_of_sockets = 0;

int main(int argc, char **argv)
{
	int sock, ret, evds;
	struct sockaddr_in client_addr;
	socklen_t client_size;
	char reqBuf[SEGSIZE];		// request buffer from UDP 69
	request req;				// TODO: request should be separated per connection

	struct kevent ev[DEFAULT_MAX_THREADS];

	//worker_pool *pool;		// a pointer to the thread pool

	sigset_t sigmask;			// signal mask

	// read in server configuration file, refer to configuration.c
	if (argc == 3 && strncmp("-c", argv[1], 2) == 0)
		read_configuration(argv[2], stderr);
	else
		read_configuration(NULL, stderr);

	// Change working directory into the TFTP_BOOT from the configuration file
	if (chdir(configuration.tftp_boot) != 0) {
		fprintf(stderr, "Unable to get server_root directory : %s\n", configuration.tftp_boot);
		perror("chdir");
		exit(1);
	}

	// Become a daemon!
	daemonize();

	// create and bind
	sock = create_bind_socket(configuration.port);
	if(sock == FAIL)
	{
		perror("create_bind_socket");
		exit(1);
	}

	// add a kevent
	evds = addKqEvent(FALSE, sock, EVFILT_READ, NULL);
	if (evds == FAIL) {
		fprintf(stderr, "addKqEvent failed\n");
		close(sock);
		exit(1);
	}else{
		num_of_sockets++;
	}

	// Drop the privilege 
	chuser();

	// fill the signal mask with all blockable signals
	sigfillset(&sigmask);

	// accept some signals
	sigdelset(&sigmask, SIGINT);	// CTRL+C
	sigdelset(&sigmask, SIGTERM);	// KILL
	sigdelset(&sigmask, SIGTSTP);	// CTRL+Z

	/*
	 * TODO: revive threadpool, make single dispatcher multi worker model
	pool = init_worker_pool();
	if (pool == NULL) {
		fprintf(stderr, "Unable to initiate thread pool.\n");
		close(sock);
		close(evds);
		exit(1);
	}
	*/


	// main loop: TODO: shorten it
	while (TRUE) 
	{
		errno = 0;
		int nev = 0; // num of event
		int serviceSock = 0;
		/*
		 *	 No change, but monitoring only
		 */	 			
		nev = kevent(evds, NULL, 0, ev, num_of_sockets, NULL);
		if(nev == FAIL) {
			syslog(LOG_ERR, "kevent main failed: %m");
			continue;
		}

		if(nev == 0) continue;

		for(int i = 0; i < nev; i++)
		{
			int result = 0;

			if (ev[i].flags & EV_EOF) {
				syslog(LOG_ERR, "client disconnected");
				rmKqEvent(evds, ev[i].ident, EVFILT_READ);
				num_of_sockets--;
				close(ev[i].ident);
				continue;
			}
		
			if(ev[i].ident == sock) // new connection
			{
				
				// clean request buffer
				bzero(reqBuf, SEGSIZE);

				// read data from client
				client_size = sizeof(client_addr);
				errno = 0;
				ret = recvfrom(sock, reqBuf, SEGSIZE, 0, (struct sockaddr *) &client_addr, &client_size);
				if (ret < 0) {
					if (errno != EAGAIN) syslog(LOG_ERR, "Error on recvfrom: %m\n");
					continue;
				}
	
				// parse the request, returns FAIL or TRUE
				ret = parseFirstRequest(&req, reqBuf, ret);
				if (ret == FAIL) {
					if (errno)
						nak(&req, errno);
					continue;
				}
				req.client = client_addr;

				// Process the new request
				// create and bind
				serviceSock = create_bind_socket(0);
				if(serviceSock == FAIL)
				{
					syslog(LOG_ERR, "Error, failed to create a new socket for a service thread");
					bzero(&req, sizeof(req));
					continue;
				}

				req.evds = serviceSock;
				if(FAIL == connect(serviceSock, (struct sockaddr *) &client_addr,client_size))
				{
					syslog(LOG_ERR, "Error, connect failed: %m");
					nak(&req, errno);
					close(serviceSock);
					bzero(&req, sizeof(req));
					continue;
				}
			
				// Add the service socket into KQ
				if(FAIL == addKqEvent(evds, serviceSock, EVFILT_READ, (void *)&req))
				{
					syslog(LOG_ERR, "Error, addKqEvent failed");
					nak(&req, errno);
					close(serviceSock);
					bzero(&req, sizeof(req));
				}else{
					num_of_sockets++;
				}

				syslog(LOG_ERR, "main sock, sock: %d, nev = %d, nos=%d\n", sock, nev, num_of_sockets);

				req.retry = 3;

				switch(req.reqType)
				{
					case RRQ:
						result = processRRQ(&req);
						if(FAIL == result)
						{
							cleanUp(evds, &req, errno);
							continue;
						}
						if(FALSE == sendOACK(&req, RECV_ACK))
							sendBlock(&req);
					break;

					case WRQ:

						if(FAIL == processWRQ(&req))
						{
							cleanUp(evds, &req, errno);
							continue;
						}
						
						// this is optional
						if(FALSE == sendOACK(&req, RECV_DATA))
							ack(&req);
					break;

					default:
						syslog(LOG_ERR, "Error: unsupported opcode: %d", req.reqType);
						cleanUp(evds, &req, EBADOP);
				}

		}else{	// ongoing sockets

			request* rq = (request *)ev[i].udata;
			if(rq->state == RECV_DATA)
			{
				// save file
				if(FAIL == recvBlock(rq))
				{
					cleanUp(evds, rq, errno);
					continue;
				}

				// ack
				ack(rq);
			}

			if(rq->state == RECV_ACK)
			{
				if(FAIL == recvBlock(rq)) {
					// TODO: retry except for OACK
					cleanUp(evds, rq, errno);
					continue;
				}

				if(rq->reqType == RRQ)
				{
					rq->blknum++;
					if(rq->server_opcode == DATA)
					{
						if(rq->chunks-- == 1) // done
						{
							cleanUp(evds, rq, -1);
							continue;
						}
					}
					syslog(LOG_ERR, "BLK:%d, CHUNKS:%d", rq->blknum, rq->chunks);
					rq->state = SEND_DATA;
				}
			}
			
			if(rq->state == SEND_DATA)
			{
				if(FAIL == sendBlock(rq)) cleanUp(evds, rq, EBADOP);		
			}
		}
	} // for
	} // while
	return ret;
}

void cleanUp(int kq, request* req, int eno)
{
	if(eno != -1) nak(req, eno);
	rmKqEvent(kq, req->evds, EVFILT_READ);
	close(req->evds);
	num_of_sockets--;
	bzero(req, sizeof(request));
}

/**
* Maximize system resources we would consume
*/
void max_resource()
{
	struct rlimit rl;

	// Core dump
	rl.rlim_cur = rl.rlim_max = RLIM_INFINITY;
	if (setrlimit(RLIMIT_CORE, &rl) == FAIL) {
		perror("setrlimit, RLIMIT_CORE");
		exit(1);
	}
	// number of open files
	rl.rlim_cur = rl.rlim_max = configuration.max_threads * 10;
	if (setrlimit(RLIMIT_NOFILE, &rl) == FAIL) {
		perror("setrlimit, RLIMIT_NOFILE");
		exit(1);
	}
	// size of actual memory
	rl.rlim_cur = rl.rlim_max = RLIM_INFINITY;
	if (setrlimit(RLIMIT_AS, &rl) == FAIL) {
		perror("setrlimit, RLIMIT_AS");
		exit(1);
	}
	// stack size for each thread, Kbyte -> byte
	rl.rlim_cur = rl.rlim_max = configuration.stacksize * 1000;
	if (setrlimit(RLIMIT_STACK, &rl) == FAIL) {
		perror("setrlimit, RLIMIT_STACK");
		exit(1);
	}
}

/**
 * Logs the PID of this server
 * @param fname a char pointer to the filename that will hold the pid
 */
void log_pid(char *fname)
{
	FILE *pid_file;				// a creek

	// open the file with writing option
	if ((pid_file = fopen(fname, "w")) == NULL) {
		// Oh, no!
		fprintf(stderr, " could not log pid to file %s\n", fname);
		perror("fopen");
		return;
	}
	// get the PID
	fprintf(pid_file, "%ld\n", (long) getpid());
	fclose(pid_file);
}

// This function makes the daemon!!!
void daemonize()
{
	// this is parent (current) process
	int i;
	pid_t pid;

	// It's already a daemon
	if (getppid() == 1)
		return;

	// Fork a child
	pid = fork();

	// DUP! fork error
	if (pid < 0) {
		fprintf(stderr, "fork ERRROR in daemonize:%s!", strerror(errno));
		exit(1);
	}
	// (current) parent exits, child now is under control of initd
	if (pid > 0)
		exit(0);

	////////////////////////// child (daemon) continues
	setsid();	// the child obtains a new process group

	// maximize required system resources
	max_resource();

	// close all descriptors to not waste resources that
	// a daemon will never use
	for (i = getdtablesize(); i >= 0; --i)
		close(i);

	// handle standard I/O
	i = open("/dev/null", O_RDWR);
	dup(i);
	dup(i);

	// set newly created file permissions(0644)
	umask(022);

	// Save the pid of this server
	log_pid(configuration.pidfile);

	syslog(LOG_INFO, "stftpd damonized!");
}

/**
 * Drops the root privilege and change the process's (e)uid into the one indicated in configuration
 */
void chuser()
{
	struct passwd *pwd;
	if ((pwd = getpwnam(configuration.runas)) == NULL)
		pwd = getpwnam("nobody");
	if (pwd == NULL) {
		syslog(LOG_ERR, "Error, no such user: neither %s nor nobody",
			   DEFAULT_USER);
		exit(1);
	}

	if (setuid(pwd->pw_uid) == FAIL) {
		syslog(LOG_ERR, "Error, setuid failed: %m");
		exit(1);
	}
}
