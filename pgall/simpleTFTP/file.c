#include "file.h"
#include <strings.h>
#include <pthread.h>

/** 
 * Check permissions and stuff then return the size of a file
 * 
 * @param filename a char type pointer to the name of a file
 * @return the size of the file in bytes
 */
off_t verifyFile(char *filename)
{
	struct stat statbuf;
	if (stat(filename, &statbuf) < 0) {
		syslog(LOG_ERR, "%s, %m\n", filename);
		return FAIL;

	} else if ((statbuf.st_mode & (S_IREAD >> 6)) == 0) {
		syslog(LOG_ERR, "Permission denied: %s\n", filename);
		return FAIL;
	}

	return statbuf.st_size;
}

/*
 * CRLF -> LF
 * CR,NUL -> CR
 */
unsigned int convertAsc(char *src, char *dst, unsigned int len)
{
	register unsigned int count = 0;
	register int c;

	while (len-- > 0) {
		c = *src++;
		switch (c) {
			case EOF:
				if (count < len)
				{
					syslog(LOG_ERR, "unexpected EOF");
					return FAIL;
				}

				return count;
			break;

			case '\r':

				if(*src == '\0')
				{
					*dst++ = c; src++;
				}else if(*src == '\n'){
					*dst++ = *src++;
				}

			break;

			default:
				*dst++ = c;
		}
		count++;
	}
	return count;
}
