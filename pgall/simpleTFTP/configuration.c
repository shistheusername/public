/*
 * Implements functions for reading server's config file.
 */
#include "configuration.h"

// constants that will make this code more readable
#define TYPE_INT 0
#define TYPE_STRING 1

// Global server configuration variable
config_rec configuration;

/* This table of directives is used to validate keywords from the config file.
 * Because I don't use numerous keywords, any advanced data structire like the hash table would be overkill.
 *
 * Items in the table are sorted in an alphabetical order, and every keyword is one letter in lowercase.
 * In other words, records are sorted in its keyword's ASCII order.
 * As long as we keep this order, find_directive() function will be fast enough to evaluate entire file 
 * in less than a second.
 */
DIRECTIVE table_of_directives[] = {

	//keyword, datatype, the actual memory space
	{"idletimeout", TYPE_INT, (void *) &(configuration.idle_timeout)},
	{"maxclients", TYPE_INT, (void *) &(configuration.max_clients)},
	{"maxidlethreads", TYPE_INT,
	 (void *) &(configuration.max_idle_threads)},
	{"maxthreads", TYPE_INT, (void *) &(configuration.max_threads)},
	{"minidlethreads", TYPE_INT,
	 (void *) &(configuration.min_idle_threads)},
	{"pidfile", TYPE_STRING, (void *) (configuration.pidfile)},
	{"port", TYPE_INT, (void *) &(configuration.port)},
	{"runas", TYPE_STRING, (void *) (configuration.runas)},
	{"stacksize", TYPE_INT, (void *) &(configuration.stacksize)},
	{"startthreads", TYPE_INT,
	 (void *) &(configuration.start_thread_count)},
	{"statinterval", TYPE_INT, (void *) &(configuration.stat_interval)},
	{"tftpboot", TYPE_STRING, (void *) (configuration.tftp_boot)},
	{"timeout", TYPE_INT, (void *) &(configuration.timeout)}
};

/*
 * Error reporting function
 *
 * This function will report any errors that may be raised 
 * while the program is reading up the configuration file.
 * 
 * error_msg is a char pointer to an error message to be displayed to a user.
 * filename is a char pointer to the name of a log file.
 * lineno is an integer indicating the # of the line that an error has been found.
 * errors is a FILE pointer to an output file stream.
 */
void
config_error(char *error_msg, char *filename, int lineno, FILE * errors)
{
	fprintf(errors, "Syntax error on line %d of %s:\n", lineno, filename);
	fprintf(errors, "%s.\n", error_msg);
	exit(1);
}

/*
 * Assign default values to the configuration
 *
 * This function assigns default values, which are defined in constants.h, 
 * to the records in the config_rec.
 * 
 * The config file does not necessarily have to contain these keywords.
 */
void setup_default_configuration()
{
	// integer type default values
	configuration.port = DEFAULT_PORT;
	configuration.start_thread_count = DEFAULT_START_THREADS;
	configuration.min_idle_threads = DEFAULT_MIN_IDLE_THREADS;
	configuration.max_idle_threads = DEFAULT_MAX_IDLE_THREADS;
	configuration.max_threads = DEFAULT_MAX_THREADS;
	configuration.max_clients = DEFAULT_MAX_CLIENTS;
	configuration.timeout = DEFAULT_TIMEOUT;
	configuration.idle_timeout = DEFAULT_IDLE_TIMEOUT;
	configuration.port = DEFAULT_PORT;
	configuration.stacksize = DEFAULT_STACKSIZE;
	configuration.stat_interval = DEFAULT_STAT_INTERVAL;

	// char * type default values
	bzero(configuration.tftp_boot, MAX_STRING_LEN);	// directory path for files to be served
	strncpy(configuration.tftp_boot, DEFAULT_TFTPBOOT,
			strlen(DEFAULT_TFTPBOOT));

	bzero(configuration.pidfile, MAX_STRING_LEN);	// path for a file to have the PID of this process
	strncpy(configuration.pidfile, DEFAULT_PIDFILE,
			strlen(DEFAULT_PIDFILE));

	bzero(configuration.runas, MAX_STRING_LEN);	// username,which the server will run as
	strncpy(configuration.runas, DEFAULT_USER, strlen(DEFAULT_USER));
}

/*
 * Function that reads up the config file
 *
 * This function scans the configuration file, reads in those keywords, and
 * assigns values to the records in the config_rec.
 * 
 * How does it work?
 *
 * 1. setup default values if possible
 * 2. opens the configuration file, which is defined in constants.h
 * 3. reads lines that are not commented
 * 4. parses the lines and verifies keywords
 * 5. assigns the value to the configuration
 *
 * errors is a FILE pointer to an output file stream.
 */
void read_configuration(const char *filename, FILE * errors)
{
	// First of all, assign default values first
	setup_default_configuration();

	FILE *fd_cfg;				// FILE stream pointer to the configuration file

	char line[MAX_STRING_LEN];	// line buffer
	int lineno = 0;				// line number 
	int line_length = 0;		// line length

	int directive_index = 0;	// the position of a valid keyword in the directive_table 
	char *directive, *value, *ptr;	// helper pointers

	if (filename == NULL) {

		if (!(fd_cfg = fopen(SERVER_CONFIG_FILE, "r"))) {
			perror(SERVER_CONFIG_FILE);
			exit(1);
		}

	} else {

		// Open the configuration file with reading option
		if (!(fd_cfg = fopen(filename, "r"))) {
			fprintf(errors, "\ncould not open configuration file: %s\n",
					filename);
			perror("fopen");
			fprintf(errors,
					"tring to read a configuration file from default path: %s\n",
					SERVER_CONFIG_FILE);
			if (!(fd_cfg = fopen(SERVER_CONFIG_FILE, "r"))) {
				fprintf(errors, "could not open configuration file: %s\n",
						filename);
				perror("fopen");
				exit(1);
			}
		}
	}

	// read lines until we get to the end
	// getline() is defined in util.c
	while (-1 !=
		   (line_length = getline(line, MAX_STRING_LEN, fd_cfg, &lineno)))
	{
		// line_length can be <= 0, if ya_cfg_getline sees an empty line
		if (line_length <= 0)
			continue;

		// if we are here, then it means we have a directive line so lineno should go up.
		++lineno;

		// Parse the directive line, which is in the form of "Directive Value"
		directive = strtok(line, " ");

		// Is the directive valid?
		if ((directive_index = find_directive(directive)) != FAIL) {
			// then get the value
			value = strtok(NULL, " ");

			// We have two different data types:TYPE_STRING or TYPE_INT
			if (table_of_directives[directive_index].type == TYPE_STRING) {
				// an alias that makes my life easier
				ptr = (char *) table_of_directives[directive_index].value;

				// clean up the buffer prior to use it
				bzero(ptr, MAX_STRING_LEN);

				// copy the value into the config_rec
				strncpy(ptr, value, strlen(value));

				// if the keyword is "directoryindex"
				if (directive_index == 0) {
					// read the rest of the line
					while (NULL != (value = strtok(NULL, " "))) {
						strncat(ptr, " ", 1);
						strncat(ptr, value, strlen(value));
					}

					// So, for the directoryindex, user can list
					// many files in the form of "file1 file2 file3 ... "
				}
				// make sure that all values get NULL terminated 
				ptr[strlen(ptr)] = '\0';

			} else {	// integer values

				// validate the integer before we actually use it.
				if (atoi(value) < 0) {
					config_error("Illegal value", SERVER_CONFIG_FILE,
								 lineno, stderr);
				}
				// now it's safe.
				*(int *) table_of_directives[directive_index].value =
					atoi(value);
			}

		} else {	// Something is wrong, dude.
			config_error("Unknown directive", SERVER_CONFIG_FILE, lineno,
						 stderr);
		}

		// clean up helpers for next iteration
		directive = value = NULL;
	}

	// Close the config file
	fclose(fd_cfg);
}

/* 
 * Function that validates the keywords
 * 
 * Given a keyword string, this function will return the index into the keyword table.
 * The function assumes that the keywords are in alphabetacal order. 
 * It will return FAIL when not found.
 *
 * search_target is a pointer to characters holding the keyword that has to be validated.
 */
int find_directive(char *search_target)
{
	// Some helper variables
	int num_directives = sizeof(table_of_directives) / sizeof(DIRECTIVE);
	int start = 0;
	int end = num_directives - 1;
	int target_index, res;

	// convert the keyword into a lowercase word for comparison
	// str2lower is defined in util.c, FYI
	str2lower(search_target);

	/* divide and conquer strategy
	 *
	 * Following 4 steps are executed:
	 *
	 * 1. pick the one in the middle from the table.
	 * 2. compare them
	 * 3. Look in lower/higher half depending on the result
	 * 4. Repeat 1-3 until either we find one or get exhausted
	 */
	while (start <= end) {

		// divide and pick the one in the middle
		target_index = (start + end) / 2;

		// compare them
		res =
			strcmp(search_target, table_of_directives[target_index].name);
		if (res < 0)	// Too little? let's try to look in lower range
		{
			end = target_index - 1;
		} else if (res == 0)	// yeah~, we found it.
		{
			return target_index;
		} else if (res > 0)	// Too much? Let's try to look in higher range
		{
			start = target_index + 1;
		}
	}

	// Duh!
	return FAIL;
}

/* 
 * Function that reports current configuration
 * 
 * This function will dump current configuration that the server is running on
 * to a file. When the server process catches SIGHUP, it's time for this function.
 * 
 * config is a pointer to config_rec indicating the configuration image that
 * this function will print out.
 */
void print_configuration(config_rec * config)
{
	// output stream pointer
	FILE *out = stdout;

	// try to open the stat_file that might be configured from the config file
	/*
	   if((out = fopen(config->stat_file,"w")) == NULL) 
	   {
	   // Complain about it
	   perror("fopen");
	   fprintf(stderr," could not open status file (%s)\n", config->stat_file);
	   fprintf(stderr," using STDOUT instead\n");

	   // we are using the standard out instead
	   out = stdout;
	   }
	 */

	fprintf(out, "Configuration \n");
	fprintf(out, "==========================================\n");
	fprintf(out, "Port : %d\n", config->port);
	fprintf(out, "TftpBoot : %s\n", config->tftp_boot);
	fprintf(out, "PidFile : %s\n", config->pidfile);
	fprintf(out, "StartThreads : %d\n", config->start_thread_count);
	fprintf(out, "MaxThreads : %d\n", config->max_threads);
	fprintf(out, "MaxClients : %d\n", config->max_clients);
	fprintf(out, "Idle TimeOut : %d\n", config->idle_timeout);
	fprintf(out, "TimeOut : %d\n", config->timeout);
	fprintf(out, "==========================================\n");

	// can' touch stdout
	if (out != stdout)
		fclose(out);
}
