#include <stdlib.h>
#include <stdio.h>

// for more readability
#define TRUE 1
#define FALSE 0
#define FAIL -1

#define FILENAME 256
#define MAX_STRING_LEN 256

#define MINTIMEOUT 1
#define MAXTIMEOUT 255

// the name of server configuration file
#define SERVER_CONFIG_FILE "/etc/stftpd.conf"

// default values for the server configuration
#define DEFAULT_PORT 69
#define DEFAULT_USER "root"

#define DEFAULT_TIMEOUT 5	// secs
#define DEFAULT_IDLE_TIMEOUT 5	// secs
#define DEFAULT_STAT_INTERVAL 10	// secs

#define DEFAULT_STACKSIZE 128	// Kbytes

#define DEFAULT_START_THREADS 5

#define DEFAULT_MIN_IDLE_THREADS 5
#define DEFAULT_MAX_IDLE_THREADS 10

#define DEFAULT_MAX_CLIENTS 5
#define DEFAULT_MAX_THREADS 10000

#define DEFAULT_PIDFILE "/var/run/stftpd.pid"
#define DEFAULT_TFTPBOOT "/tftpboot"
