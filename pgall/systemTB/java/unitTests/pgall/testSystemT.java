package pgall;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import pgall.datatypes.TLV;

public class testSystemT {

	SystemT target;
	
	@Before
	public void setUp() throws Exception {
		target = new SystemT();
	}
	
	@Test
	public void testCreateArrayTLV()
	{
		String line = "\"phoneNumbers\": [ { \"type\": \"mobile\", \"number\": \"123 555-1237\" }, { \"type\": \"mobile\", \"number\": \"123 555-1237\" } ]";
		TLV tlv = target.createArrayTLV(line);
		Assert.assertNotNull(tlv);
		
		tlv = null;
		
		line = "\"phoneNumbers\": [ { \"type\": \"mobile\", \"number\": \"123 555-1237\" } ]";
		tlv = target.createArrayTLV(line);
		Assert.assertNotNull(tlv);
	}
	
	@Test
	public void testCreateComplexTLV()
	{
		String line = "{\n" +
        "\"name\": \"Santa\",\n" +
        "\"age\": 0,\n" +
        "\"address\": \"North pole HOHOHO\","+

        "\"pet\": { \n"+
		"\"name\": \"rudolph\",\n"+ 
		"\"species\": \"reindeer\" \n" +
		"},\n" +

        "\"language\": [\n"+ 
		"\"en\",\n"+ 
		"\"ko\", \n"+
		"\"ru\" \n" +
		"],\n"+

		"\"isAlive\": false\n}";
		
		TLV tlv = target.createComplexTLV(line);
		Assert.assertNotNull(tlv);
	}
	
	
	@Test
	public void testReadData(){
		try {
//			target.readData("C:\\Users\\E78417\\Downloads\\fun\\systemTB\\exampleT.out");
//			target.transform();
//			target.printTofile();
			
			target.readData("C:\\Users\\E78417\\Downloads\\fun\\systemTB\\systemT.out");
			target.transform();
			target.printTofile();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testIsBrokenPart()
	{
		boolean actual = Util.isBrokenPart("\"age\": 34");
		Assert.assertEquals(false, actual);
		
		actual = Util.isBrokenPart("\"address\": \"yogi");
		Assert.assertEquals(true, actual);
	}
	
	@Test
	public void testGetByteLen() {
		Assert.assertEquals(1, Util.getByteLen(254));
		Assert.assertEquals(1, Util.getByteLen(-1));
		Assert.assertEquals(2, Util.getByteLen(32767));
		Assert.assertEquals(2, Util.getByteLen(-32768));
		Assert.assertEquals(3, Util.getByteLen(65536));
		Assert.assertEquals(4, Util.getByteLen(Integer.MAX_VALUE));
		Assert.assertEquals(4, Util.getByteLen(Integer.MIN_VALUE));
	}
}
