package pgall;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class testSystemB {

	SystemB sB;
	SystemT sT;
	
	@Before
	public void setUp() throws Exception {
		sB = new SystemB();
		sT = new SystemT();
	}

	@Test
	public void testBT(){
		try {
			
			sT.readData("C:\\Users\\E78417\\Downloads\\fun\\systemTB\\systemT.in");
			sT.transform();
			sT.printTofile();
			
			sB.readData("C:\\Users\\E78417\\Downloads\\fun\\systemTB\\systemB.in");
			sB.transform();
			sB.printTofile();
			
			sT.readData("C:\\Users\\E78417\\Downloads\\fun\\systemTB\\systemT.in");
			sT.transform();
			sT.printTofile();
			
			sB.readData("C:\\Users\\E78417\\Downloads\\fun\\systemTB\\systemB.in");
			sB.transform();
			sB.printTofile();
		
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
}
