package pgall;

/**
 * Implements various utility functions
 * @author sh
 *
 */
public class Util {

	/**
	 * Finds out required # of bytes for given integer
	 * @param x integer
	 * 
	 * @return # of bytes
	 */
	public static int getByteLen(int x)
	{
		if(x >= 0)
		{		
			int y = x & 0xFF000000;
			if(y != 0) return 4;
			
			y = x & 0x00FF0000;
			if(y != 0) return 3;
			
			y = x & 0x0000FF00;
			if(y!=0) return 2;
			
			return 1;
		}else{
			if(x >= -128) return 1;
			if(x >= -32768) return 2;
			return 4;
		}
	}
	
	/**
	 * Removes enclosing " out of given string
	 * 
	 * @param str "string"
	 * @return string
	 */
	public static String stripEnclosures(String str)
	{
		try{
			 return str.substring(str.indexOf('"')+1, str.lastIndexOf('"'));
		}catch(IndexOutOfBoundsException e)
		{
			return str;
		}
	}
	
	/**
	 * byte[] to HexString
	 * @see http://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
	 */
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {	
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	/**
	 * Check if given string is a broken token such as "broken
	 * 
	 * @param part a string
	 * @return true if there is odd # of " in the string
	 */
	public static boolean isBrokenPart(String part)
	{
		int numOfBracket = 0;
		for(char c: part.toCharArray())
		{
			if(c == '"') numOfBracket++;
		}
		return ((numOfBracket & 0x01) == 1);
	}
}
