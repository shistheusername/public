package pgall;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import pgall.datatypes.SimpleType;
import pgall.datatypes.ComplexType;
import pgall.datatypes.NamedSimpleType;
import pgall.datatypes.TLV;
import pgall.datatypes.Types;

/**
 * Represents SystemT transformer: SystemB file -> SystemT file
 * @author sh
 *
 */
public class SystemT {

	String json;
	TLV convertedTLV;

	public void readData(String filename) throws Exception
	{
		json = new Scanner(new File(filename)).useDelimiter("\\Z").next();
	}

	public void transform()
	{
		convertedTLV = createComplexTLV(json);
	}

	/**
	 * Creates a complex type TLV
	 * 
	 * @param line should be { ... }
	 * @return complex TLV
	 */
	protected TLV createComplexTLV(String line)
	{		
		TLV tlv = null; 
		if(line.startsWith("{"))
		{
			tlv = new ComplexType(Types.COMPLEX);
			
			String[] parts = line.split(",");
			for(int i = 0; i < parts.length; i++)
			{	
				String part = parts[i].trim();
				int numOpenBracket = 0;
				if(part.contains("[")) // array
				{
					TLV arrayTlv = null;
					if((part.indexOf(']') - part.indexOf('[')) == 1) // empty array
					{
						arrayTlv = createArrayTLV(part);
					}else{
						numOpenBracket = 1;
						StringBuilder sb = new StringBuilder();
						sb.append(part);
						do {
							part = parts[++i].trim();
							sb.append(",").append(part);
							
							if(part.contains("[")) numOpenBracket++;
							if(part.contains("]")) numOpenBracket--;
							
						}while(numOpenBracket >= 1 || (!part.endsWith("],") &&  !part.endsWith("]")));
						
						arrayTlv = createArrayTLV(sb.toString());
					}
					if(null != arrayTlv) tlv.addChild(arrayTlv);
					
				}else if(part.contains("{") && i > 0){ // complex 
					
					numOpenBracket = 1;
					StringBuilder sb = new StringBuilder();
					sb.append(part);
					do {
						part = parts[++i].trim();
						sb.append(",").append(part);
						
						if(part.contains("{")) numOpenBracket++;
						if(part.contains("}")) numOpenBracket--;
						
					}while(numOpenBracket >= 1 || (!part.endsWith("},") &&  !part.endsWith("}")));
					
					TLV complexTlv = createComplexTLV(sb.toString());
					if(null != complexTlv) tlv.addChild(complexTlv);
					
				}else{// simple types
						
					if(Util.isBrokenPart(part))
					{
						StringBuilder sb = new StringBuilder(part);
						do {
							part = parts[++i].trim();
							sb.append(",").append(part);
						}while(!part.endsWith("\",") && !part.endsWith("\""));
						
						part = sb.toString();
					}
					
					TLV[] subs = createTLV(part);
					for(TLV t: subs)
						if(null!=t) tlv.addChild(t);
				}
			}
		}else{ // named complex
			
			String[] parts = line.split("\\{");
			String varName = parts[0].trim();
			tlv = new ComplexType(Types.COMPLEX, varName);

			parts = parts[1].trim().split(",");
			for(String part : parts)
			{
				part = part.trim();
				
				TLV[] subTlv = createTLV(part);
				for(TLV t: subTlv)
					if(null != t) tlv.addChild(t);		
			}
		}

		return tlv;
	}
	
	/**
	 * Creates an Array type TLV
	 * 
	 * @param line should be "name": [ .. ],?
	 * @return Array/PhoneNumber TLV
	 */
	protected TLV createArrayTLV(String line)
	{
		ComplexType array = null;
		if(!line.startsWith("[")) // named array
		{
			int brackStart = line.indexOf('[');
			
			String varName = line.substring(0,brackStart);
			Types type = Types.getTypeCode(varName);
			
			array = (Types.PHONENUMS == type) ? new ComplexType(type) : new ComplexType(Types.ARRAY, varName);
			String valuePart = line.substring(brackStart+1).trim();

			if(valuePart.startsWith("{")) // complex elements
			{
				String[] elements = valuePart.split("\\},");
				for(String e: elements)
				{
					TLV subTlv = createComplexTLV(e.trim());
					if(null != array) array.addChild(subTlv);
				}
				
			}else{
				
				if(!valuePart.equals("]")) // empty Array
				{
					String[] parts = valuePart.split(",");
					for(String part : parts)
					{
						TLV[] tlv = createTLV(part.trim());
						for(TLV t: tlv)
							if(null != t) array.addChild(t);
					}
				}
			}
		}
		
		return array;
	} 

	/**
	 * Creates a non-complex TVL
	 * 
	 * @param line should be "var": "?val"?
	 * @return a simple type TLV
	 */
	protected TLV[] createTLV(String line)
	{		
		TLV[] result = null;
		
		String[] vvPair = line.split(":");
		result = new TLV[vvPair.length];
		
		if(vvPair.length >= 2) // "var":"val"
		{
			String varName = vvPair[0].trim();
			Types varType = Types.getTypeCode(varName);
			String value = vvPair[1].trim();
		
			if(Types.NAME == varType || Types.ADDRESS == varType)
				result[0] = new SimpleType(varType, value);

			if(Types.STRING == varType)
			{
				// Boolean type
				if(value.contains("true") || value.contains("false"))
				{
					varType = Types.BOOLEAN;
					result[0] = new NamedSimpleType(varType, varName, value);
				}else{
					result[0] = new SimpleType(varName);
					result[1] = new SimpleType(value);
				}
			}

			if(Types.AGE == varType)
			{
				int commaIdx = value.lastIndexOf(',');
				if(commaIdx > 0)
					value = value.substring(0, commaIdx);
				result[0] = new SimpleType(varType, Integer.parseInt(value));
			}

		}else{
			result[0] = new SimpleType(line);
		}
		
		return result;
	}

	public void printTofile() throws IOException
	{
		FileOutputStream os = new FileOutputStream("systemB.in");
		convertedTLV.toBinary(os, false);
		os.close();
	}
}
