package pgall;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;

import pgall.datatypes.SimpleType;
import pgall.datatypes.ComplexType;
import pgall.datatypes.NamedSimpleType;
import pgall.datatypes.TLV;
import pgall.datatypes.Types;

/**
 * Represents SystemB transformer: SystemT file -> SystemB file
 * @author sh
 *
 */
public class SystemB {
	ByteBuffer bb;
	TLV convertedTLV;

	public void readData(String filename) throws IOException {
		byte[] data = Files.readAllBytes(Paths.get(filename));
		bb = ByteBuffer.wrap(data);
	}

	public void transform() {
		Types type = Types.getType(bb.get());
		convertedTLV = createComplexTLV(type, false, false);			
	}

	/**
	 * Creates complex type TLV: Complex, Array, and PhoneNumber
	 * 
	 * @param type
	 * @param hasName true if this object should have a name
	 * @param inArray true if this object is a sub element of an array
	 * 
	 * @return complex TLV
	 */
	private TLV createComplexTLV(Types type, boolean hasName, boolean inArray) {

		TLV tlv = null;

		// when in an array, no name allowed
		if(hasName && !inArray)
		{
			bb.get(); //  type of name
			byte[] byteArr = new byte[bb.get()];
			bb.get(byteArr);
			tlv = new ComplexType(type, new String(byteArr));
		}else{
			tlv = new ComplexType(type);
		}

		while(bb.hasRemaining())
		{
			Types subType = Types.getType(bb.get());
			if(subType == Types.EOC) break;
			TLV child = null;
			switch(subType)
			{
			case COMPLEX:
				child = createComplexTLV(subType, true, inArray);
				break;

			case PHONENUMS:
			case ARRAY:
				child = createArrayTLV(subType);
				break;

			default:
				child = createTLV(subType);
				break;
			}
			if(child != null) tlv.addChild(child);
		}
		return tlv;
	}

	/**
	 * Creates an Array type TLV: Array and PhoneNumber
	 * 
	 * @param type
	 * @return Array/PhoneNumber type complex TLV
	 */
	private TLV createArrayTLV(Types type) {
		TLV tlv = null;

		if(type == Types.ARRAY)
		{
			// name
			bb.get(); //  type of name
			byte[] byteArr = new byte[bb.get()];
			bb.get(byteArr);
			tlv = new ComplexType(type, new String(byteArr));
		} else{ // phoneNumbers
			tlv = new ComplexType(type);
		}
		
		// length
		int length = bb.get();
		if(length == 0)
		{
			bb.get(); // type
			bb.get(); // EOC
		}else{
			
			Types subType = Types.getType(bb.get());
			// elements
			while(length-- > 0)
			{
				switch(subType)
				{
				case COMPLEX: // array element complex shouldn't have name
					TLV complextlv = createComplexTLV(subType, false, true);
					tlv.addChild(complextlv);
					break;

				default:
					TLV subTlv = createTLV(subType);
					tlv.addChild(subTlv);
					break;		
				}
			}
			bb.get(); // EOC
		}

		return tlv;
	}

	/**
	 * Creates non-complex TLV
	 * 
	 * @param type
	 * @return simple type TLV
	 */
	private TLV createTLV(Types type)
	{
		TLV tlv = null;

		int length = bb.get();
		if(type == Types.AGE || type == Types.INT)
		{
			int value = 0;
			switch(length)
			{
			case 1:
				value = bb.get();
				break;
			case 2:
				value = bb.getShort();
				break;
			case 3:
			case 4:
				value = bb.getInt();
				break;

			default:
				throw new RuntimeException("Unsupported integer length:" + length);		
			}
			tlv = new SimpleType(type, value);

		}else if(type == Types.BOOLEAN){

			length = bb.get(); // var length
			byte[] byteArr = new byte[length];
			bb.get(byteArr);
			String varName = new String(byteArr);
			tlv = new NamedSimpleType(type, varName, new String(new byte[]{bb.get()}));

		}else{
			
			byte[] byteArr = new byte[length];
			bb.get(byteArr);
			String value = new String(byteArr);
			tlv = new SimpleType(type, value);
		}

		return tlv;
	}

	public void printTofile() throws IOException {
		
		StringBuilder sb = new StringBuilder();
		sb.append(convertedTLV.toJson(0));
		sb.deleteCharAt(sb.lastIndexOf(","));
		
		FileWriter fw = new FileWriter("systemT.in");
		fw.write(sb.toString());
		fw.close();
	}
}
