package pgall;

public class Transform {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if(2 != args.length)
			usage();

		try{
			if("-tb".equals(args[0]))
			{
				SystemT systemT = new SystemT();
				systemT.readData(args[1]);
				systemT.transform();
				systemT.printTofile();
			}else if("-bt".equals(args[0]))
			{
				SystemB systemB = new SystemB();
				systemB.readData(args[1]);
				systemB.transform();
				systemB.printTofile();
			}else{
				usage();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void usage()
	{
		System.out.println("Usage: transform -tb|bt " + "inputfile");
		System.exit(1);
	}
}
