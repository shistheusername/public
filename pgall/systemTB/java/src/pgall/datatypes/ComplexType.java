package pgall.datatypes;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import pgall.Util;

/**
 * Represents Complex, Array, and phoneNumber types
 * 
 * @author sh
 */
public class ComplexType extends NamedSimpleType {

	private String name = null;
	protected List<TLV> values = null;
	
	public ComplexType(Types varType)
	{
		super(varType);
	}
	
	public ComplexType(Types varType, String varName) {
		super(varType);
		name = Util.stripEnclosures(varName);
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		if(name != null) sb.append("\nName: ").append(name).append(", ");
		
		if(null!=values)
		{
			if(type == Types.ARRAY || type == Types.PHONENUMS)
			{
				sb.append(", element #:" + length).append(", type:" + values.get(0).getTypeCode());
			}
			
			sb.append("\n");
			for(TLV t: values)
				if(null != t) sb.append(t.toString()).append("\n");
		}else{
			sb.append("{}\n");
		}
		return sb.toString();
	}
	
	public List<TLV> getChildren()
	{
		return this.values;
	}
	
	public boolean isArray()
	{
		return (type == Types.ARRAY || type == Types.PHONENUMS);
	}
	
	@Override
	public void addChild(TLV child) {
		if(null == values)
			values = new ArrayList<TLV>();
		
		values.add(child);
		
		if(isArray()) length += 1;
	}
	
	@Override
	public void toBinary(OutputStream os, boolean omitType) throws IOException {
		
		int bufferLen = 0;
		ByteBuffer b = null;
		
		switch(type)
		{
		case COMPLEX:
			// named
			if(null != this.name)
			{
				int nameLen = this.name.length();
				bufferLen = nameLen + 3; // Type code, name-T, name-L
				
				b = ByteBuffer.allocate(bufferLen);
				
				// name
				if(!omitType) b.put(this.type.getCode());
				b.put(Types.STRING.getCode()).put((byte)nameLen);
				b.put(this.name.getBytes(StandardCharsets.US_ASCII));
				
			}else{ // anonymous 
				if(!omitType){ // means this in an array
					b = ByteBuffer.wrap(new byte[]{ type.getCode() });
				}else{
					omitType = false; // no propagation to sub elements
				}
			}
			break;
			
		case ARRAY:
			
			int nameLen = this.name.length();
			bufferLen = nameLen + 5; //Type code, name-T, name-L, element-#, element-type
			
			b = ByteBuffer.allocate(bufferLen);
			b.put(this.type.getCode()); // Type code
			
			// name
			b.put(Types.STRING.getCode()).put((byte)nameLen);
			b.put(this.name.getBytes(StandardCharsets.US_ASCII));
			
			// element-#
			b.put((byte)this.length);
			
			// element-type
			if(this.length > 0) b.put(values.get(0).getTypeCode());
			omitType = true;
			break;
			
		case PHONENUMS:
			bufferLen = 3; // Type code, element-#, element-type
			b = ByteBuffer.allocate(bufferLen);
			b.put(this.type.getCode());
			
			// element-#
			b.put((byte)this.length);
			
			// element-type
			if(this.length > 0) b.put(values.get(0).getTypeCode());
			omitType = true;
			
			break;
		}
		if(null != b) os.write(b.array());
	
		// sub elements
		if(values!=null)
		{
			for(TLV t: values)
				t.toBinary(os, omitType);
		}
		
		// end
		os.write(0x00);
	}
	
	@Override
	public String toJson(int depth) throws IOException {
		
		StringBuffer sb = new StringBuffer();
		
		String closingBracket = "],";
		int myDepth = depth;
		while(myDepth-->0) sb.append("\t");
		switch(type)
		{
		case COMPLEX:
			if(null != name)
				sb.append('"').append(name).append("\": ");
			
			sb.append("{");
			closingBracket = "},";
			break;
			
		case ARRAY:
			sb.append('"').append(name).append("\": [");
			break;
			
		case PHONENUMS:
			sb.append(type.label).append(": [");
			break;
		}
		
		if(null!=values)
		{
			sb.append("\n");
			boolean strVar = true;
			for(TLV t : values)
			{
				if(
					(type == Types.ARRAY && t.getTypeCode() != Types.COMPLEX.getCode()) || 
 						(t.getTypeCode() == Types.STRING.getCode() && strVar)
				)
				{
					myDepth = depth;
					while(myDepth-->=0) sb.append("\t");
				}
				sb.append(t.toJson(depth+1));
				if(t.getTypeCode() == Types.STRING.getCode())
				{
					if(type != Types.ARRAY && strVar) sb.append(": ");
					else {
						sb.append(",\n");
					}
					
					strVar = !strVar;
				}
			}
			sb.deleteCharAt(sb.lastIndexOf(","));
		
			myDepth = depth;
			while(myDepth-->0) sb.append("\t");
		}	
		sb.append(closingBracket);
		sb.append("\n");
		return sb.toString();
	}
}
