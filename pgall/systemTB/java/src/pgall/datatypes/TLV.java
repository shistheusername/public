package pgall.datatypes;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * 
 * Defines a representative type for TLV
 * @author sh
 *
 */
public interface TLV {
	public String toString();
	public void toBinary(OutputStream os, boolean omitType) throws IOException;
	public String toJson(int depth) throws IOException;
	public byte getTypeCode();
	public void addChild(TLV tlv);
	public List<TLV> getChildren();
}
