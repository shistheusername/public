package pgall.datatypes;

/**
 * Defines kinds of type
 * 
 * @author sh
 *
 */
public enum Types {
	
	EOC(0), 
	NAME(1,"\"name\""), AGE(2,"\"age\""), ADDRESS(3,"\"address\""), PHONENUMS(8,"\"phoneNumbers\""),
	COMPLEX(4), ARRAY(5), STRING(6), INT(7),
	BOOLEAN(9);
		
	private byte code;
	String label = null;

	Types(int code)
	{
		this.code = (byte)code;
	}
	
	Types(int code, String label)
	{
		this.code = (byte)code;
		this.label = label;
	}
	
	public byte getCode()
	{
		return code;
	}
	
	public static Types getTypeCode(String data)
	{
		if("true".equals(data) || "false".equals(data)) return BOOLEAN;
		
		for(Types t : Types.values())
			// so that less string cut needed
			if(null!=t.label && data.contains(t.label)) return t;
		
		return STRING; // default data type
	}
	
	public static Types getType(byte code)
	{
		for(Types t : Types.values())
			if(t.code == code) return t;
		
		throw new RuntimeException("Unknown type code:" + code);
	}
}
