package pgall.datatypes;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;

import pgall.Util;
/**
 * Defines a SimpleType TLV: type, length, and value
 * 
 * @author sh
 *
 */
public class SimpleType implements TLV{
	
	protected Types type;
	protected int length = 0;
	
	protected String strVal = null;
	protected int intVal = 0;
	
	public SimpleType(Types type)
	{
		this.type = type;
	}
	
	public SimpleType (String str) //"str"
	{
		type = Types.getTypeCode(str);
		setStringValue(str);
		length = strVal.length();
	}
	
	public SimpleType(Types varType, String str) {
		this.type = varType;
		setStringValue(str);
		length = this.strVal.length();
	}
	
	public SimpleType(Types varType, int val) {
		this.type = varType;
		this.intVal = val;
		this.length = Util.getByteLen(val);
	}
	
	public byte getTypeCode()
	{
		return type.getCode();
	}
	
	public void setStringValue(String val)
	{
		this.strVal = Util.stripEnclosures(val);
	}
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Type: ").append(type).append(", ");
		sb.append("Length: ").append(length).append(", ");
		sb.append("Value: ");
		if(type == Types.INT || type == Types.AGE)
			sb.append(intVal);
		else 
			sb.append(strVal);
		
		return sb.toString();
	}
	
	@Override
	public void toBinary(OutputStream os, boolean omitType) throws IOException {
		if (this.type == Types.COMPLEX)
			os.write(new byte[]{ type.getCode() });
		
		int bufferLen = this.length + 2;
		if(omitType) bufferLen -= 1;

		ByteBuffer b = ByteBuffer.allocate(bufferLen);
		if(!omitType) b.put(this.type.getCode());
		b.put((byte)this.length);
		
		if(type == Types.AGE)
		{
			putIntegerValue(b);
		} else {
			b.put(strVal.getBytes(StandardCharsets.US_ASCII));
		}
		os.write(b.array());
	}

	protected void putIntegerValue(ByteBuffer b)
	{
		switch(this.length)
		{
		case 1: //Byte
			b.put((byte) intVal);
			break;
		case 2: // short
			b.putShort((short) intVal);
			break;
		case 3: // Integer
		case 4:
			b.putInt(intVal);
			break;
		}
	}
	
	@Override
	public void addChild(TLV tlv) {}

	@Override
	public String toJson(int depth) throws IOException {
		StringBuffer sb = new StringBuffer();
		switch(type)
		{
		case AGE:
			while(depth-->0) sb.append("\t");
			sb.append(type.label).append(": ");
			sb.append(intVal).append(",\n");
			break;

		case ADDRESS:
		case NAME:
			while(depth-->0) sb.append("\t");
			sb.append(type.label).append(": ");
			sb.append('"').append(strVal).append('"').append(",\n");
			break;
			
		case STRING:
			sb.append('"').append(strVal).append('"');
			break;
		}
		
		return sb.toString();
	}

	@Override
	public List<TLV> getChildren() {
		return null;
	}
}
