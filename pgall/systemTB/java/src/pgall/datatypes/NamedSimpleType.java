package pgall.datatypes;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import pgall.Util;

/**
 * Defines a SimpleType with name attribute: BOOLEAN or INT
 * @author sh
 *
 */
public class NamedSimpleType extends SimpleType {
	
	private String name = null;
	
	public NamedSimpleType(Types varType)
	{
		super(varType);
	}
	
	public NamedSimpleType(Types varType, String varName) {
		super(varType);
		name = Util.stripEnclosures(varName);
	}
	
	public NamedSimpleType(Types varType, String varName, String value) {
		super(varType);
		this.name = Util.stripEnclosures(varName);
		if(Types.BOOLEAN == varType){
			value = value.substring(0,1).toUpperCase();
			length = 1;
		}
		setStringValue(value);
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("Name: ").append(name).append(", ");
		sb.append(super.toString());
		return sb.toString();	
	}
	
	@Override
	public void toBinary(OutputStream os, boolean omitType) throws IOException {
		
		int nameLen = this.name.length();
		int bufferLen = omitType ? nameLen + 3 : nameLen + 4;
		
		ByteBuffer b = ByteBuffer.allocate(bufferLen);
		if(!omitType) b.put(this.type.getCode());
		b.put(Types.STRING.getCode()).put((byte)nameLen);
		b.put(this.name.getBytes(StandardCharsets.US_ASCII));
		
		if(type == Types.BOOLEAN){
			b.put(this.strVal.getBytes(StandardCharsets.US_ASCII));
		}else if(type == Types.INT){
			putIntegerValue(b);
		}
		os.write(b.array());
	}
	
	@Override
	public String toJson(int depth) throws IOException {
		StringBuffer sb = new StringBuffer();
		
		while(depth-->0) sb.append("\t");
		sb.append('"').append(name).append('"').append(": ");
		if(type == Types.BOOLEAN)
		{
			if(strVal.startsWith("T") )sb.append("true");
			else sb.append("false");
		}else if(type == Types.INT){
			sb.append(intVal);
		}
		sb.append(",");
		sb.append("\n");
		return sb.toString();
	}
}
