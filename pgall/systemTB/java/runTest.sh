#!/bin/sh

for FILE in exampleT.out systemT.out
do
	echo "Step1: testing -tb with ../${FILE}"
	java -jar transform.jar -tb ../${FILE}
	[ $? -ne 0 ] && echo "Test failed step1" && exit 1

	echo "Step2: testing -bt with systemB.in Step1"
	java -jar transform.jar -bt systemB.in
	[ $? -ne 0 ] && echo "Test failed step2" && exit 1

	echo "Step3: cat systemT.in created in Step2"
	cat systemT.in
	[ $? -ne 0 ] && echo "Test failed step3" && exit 1

	echo "Step4: cp systemB tp systemB0"
	cp systemB.in systemB0.in
	[ $? -ne 0 ] && echo "Test failed step4" && exit 1

	echo "Step5: testing -tb with systemT.in"
	java -jar transform.jar -tb systemT.in
	[ $? -ne 0 ] && echo "Test failed step5" && exit 1

	echo "Step6: compaing systemB0 with systemB.in"
	diff systemB0.in systemB.in
	[ $? -ne 0 ] && echo "Test failed step6" && exit 1

	echo
	echo
done
