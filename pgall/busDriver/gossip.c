#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

typedef struct _driver {
	int name;
	int numGossips;
	int numStops;
	int currentStopOffset;
	int loose1min;
	int* gossips;
	int* route;

	struct _driver* next;
} driverType;

typedef driverType* Driver;

typedef struct {
	int numDrivers;
	Driver begin;
} driverTableType;

#define GETCURRENTSTOP(X) (X->route[X->currentStopOffset])
driverTableType driverTable = {0, NULL};

void findGossipStops();
void moveToNextStop(Driver);
void readData(char*);
Driver createDriver(char* );
void destroyDriver(Driver);
int findNumberOfStops(char*);
int shareGossip(Driver, Driver);
int setGossipTable(Driver);
void printDriver(Driver);

int main(int argc, char** argv)
{
	if(argc != 2) {

		printf("Usage: %s datafile\n", argv[0]);
		exit(1);
	}

	readData(argv[1]);
	findGossipStops();
	destroyDriver(driverTable.begin);

	return 0;
}

void findGossipStops()
{
	Driver tmp;
	Driver drivers[driverTable.numDrivers];
	
	tmp = driverTable.begin;
	int i = 0;
	do{
		drivers[i++] = tmp;
	}while((tmp=tmp->next) != NULL);

	int numDriversWithAllGossips = 0; 
	for(int m = 1; m <= 480;++m) // minutes passing
	{
		for(int x = 0; x < driverTable.numDrivers-1; ++x)
		{
			for(int y = x+1; y < driverTable.numDrivers; ++y)
			{
				// Two drivers are at the same stop
				if(GETCURRENTSTOP(drivers[x]) == GETCURRENTSTOP(drivers[y]))
				{
					// both drivers know all
					if(drivers[x]->numGossips == driverTable.numDrivers && 
						drivers[y]->numGossips == driverTable.numDrivers)
						continue;

					if(0 == setGossipTable(drivers[x]))
					{

						printf("Critical: failed to setup gossipTable for driver %d\n", drivers[x]->name);
						return;
					}

					if(0 == setGossipTable(drivers[y]))
					{

						printf("Critical: failed to setup gossipTable for driver %d\n", drivers[y]->name);
						return;
					}

					numDriversWithAllGossips += shareGossip(drivers[x], drivers[y]);
					if(numDriversWithAllGossips == driverTable.numDrivers)
					{
						printf("%d\n", m);
						return;
					}
				}
			}
		}

		moveToNextStop(driverTable.begin);
	}
	printf("never\n");
}

void printDriver(Driver d)
{
	printf("Driver[%d], numGossips:%d, offset: %d\n", d->name, d->numGossips, d->currentStopOffset);
	printf("gossips:\n");
	for(int i = 0; i<driverTable.numDrivers; ++i)
	{
		printf("[%d] = %d\n",i,d->gossips[i]);
	}
}

int shareGossip(Driver x, Driver y)
{
	int fullyShared = 0;
	int wasXfull = (x->numGossips == driverTable.numDrivers) ? 1 : 0;
	int wasYfull = (y->numGossips == driverTable.numDrivers) ? 1 : 0;


	for(int i = 0; i < driverTable.numDrivers; i++)
	{
		// Y tells X
		if(x->gossips[i] == -1 && y->gossips[i] != -1)
		{
			x->numGossips+=1;
			x->gossips[i] = y->gossips[i];
			y->loose1min = 1;
		}

		// X tells Y
		if(y->gossips[i] == -1 && x->gossips[i] != -1)
		{
			y->numGossips+=1;
			y->gossips[i] = x->gossips[i];
			x->loose1min = 1;
		}
	}
	
	if(!wasXfull && (x->numGossips == driverTable.numDrivers)) fullyShared += 1;
	if(!wasYfull && (y->numGossips == driverTable.numDrivers)) fullyShared += 1;

	return fullyShared;
}

int setGossipTable(Driver d)
{
	if(NULL==d->gossips)
	{
		d->gossips = (int *)malloc(sizeof(int) * driverTable.numDrivers);
		if(NULL == d->gossips) return 0;
		memset(d->gossips, -1, sizeof(int)*driverTable.numDrivers);
		d->gossips[d->name] = 1;
	}

	return 1;
}

void moveToNextStop(Driver driver)
{
	if(driver == NULL) return;

	if(1 == driver->loose1min) {
		driver->loose1min = 0;
	}else{
		driver->currentStopOffset += 1;
		if(driver->currentStopOffset >= driver->numStops)
			driver->currentStopOffset = 0;
	}	
	moveToNextStop(driver->next);
}

void readData(char* filename)
{
	char* line = NULL;
	size_t len = 0;
	Driver driver = NULL;
	int no = 0;

	FILE* fp = fopen(filename, "r");
	if(NULL==fp)
	{
		perror(NULL);
		exit(1);
	}

	while (getline(&line, &len, fp) != -1) 
	{
		driver=createDriver(line);
		if(NULL==driver)
		{
			printf("Failed to create a driver[%d]\n", no);
			continue;
		}
		driver->name = no++;
		if(0 != driverTable.numDrivers++)
			driver->next = driverTable.begin;

		driverTable.begin = driver;
	}

	free(line);
	fclose(fp);
}

Driver createDriver(char* line)
{

	char* token = NULL;
	char* delim = " ";

	Driver driver = (Driver) malloc(sizeof(driverType));
	if(driver == NULL) return NULL;

	driver->numGossips = 1;
	driver->numStops = findNumberOfStops(line);
	driver->currentStopOffset = 0;
	driver->loose1min = 0;
	driver->gossips = NULL;
	driver->route = (int*) malloc(sizeof(int)*driver->numStops);
	if(driver->route == NULL) {
		free(driver);
		return NULL;
	}

	token = strtok(line, delim);
	int* r = driver->route;
	while(NULL != token)
	{
		*r++ = atoi(token);
		token = strtok(NULL, delim);
	}
	driver->next = NULL;

	return driver;
}


void destroyDriver(Driver d)
{
	if(NULL!=d)
	{
		if(NULL!=d->route) free(d->route);
		if(NULL!=d->gossips) free(d->gossips);
		free(d);
		destroyDriver(d->next);
	}

	return;
}

int findNumberOfStops(char* line)
{
	int number = 1;
	char *p=line;
	
	while(NULL != p && '\n' != *p){
		if(*p++ == ' ') number++;
	}

	return number;
}
