package founja.server;

import java.io.File;

public class Driver {

	static Server server;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		daemonize();
		startServer();

	}

	private static void startServer() {
		server = new Server();
		server.start();
		try {
			server.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void daemonize()
	{
		// TODO clean up
	   new File(System.getProperty("pidfile")).deleteOnExit();
	   addDaemonShutdownHook();
	   
	   System.out.close();
	   System.err.close();
	}
	
	static void shutdown()
	{
		server.halt();
	}
	
	static void addDaemonShutdownHook()
	{
	   Runtime.getRuntime().addShutdownHook(new Thread() { public void run() { Driver.shutdown(); }});
	}
}
