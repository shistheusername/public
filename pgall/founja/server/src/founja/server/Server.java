package founja.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;

public class Server extends Thread {

	private ServerSocketChannel server;
	private Selector selector;
	
	public void start()
	{
		// open server socket
		try {
			setupServerSocket();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// main loop
	}
	
	private void setupServerSocket() throws IOException
	{
		server = ServerSocketChannel.open();
		selector = Selector.open();
		
		// TODO: get the port from outside
        server.socket().bind(new InetSocketAddress(2917));
        server.configureBlocking(false);
        server.register(selector, SelectionKey.OP_ACCEPT);

	}
	public void halt() {
		try {
			
			selector.close();
			server.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
