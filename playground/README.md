## What is this ##
* This is my [Go](https://golang.org/) playground, which contains stuff I wrote in Go for learning

## Index ##
1. djikstra
- Infamous shortest path finding algorithm

2. myDNS
- A very simple local DNS cache

3. slb
- A small HTTP load balancer 