package main

import (
	"net"
	"fmt"
	"io/ioutil"
	"log"
	"gopkg.in/yaml.v2"
)

type Cache map[string][]byte

type Config struct {
	Listen  string `yaml:"listen"`
	Timeout int    `yaml:"timeout"`
	Target  string `yaml:"target"`
}

type Socket struct {
	conn *net.UDPConn
	addr *net.UDPAddr
	isClient bool
}

func (s *Socket) send(buf []byte) int {
		var n int
		var err error

		if s.isClient {
			n, err = s.conn.WriteToUDP(buf, s.addr)
		} else {
			n, err = s.conn.Write(buf)
		}

        if err != nil {
            fmt.Println("Error: ", err)
        } 

        fmt.Println(n, "bytes Sent to", s.addr)
		return n
}

func (s *Socket) recv(buf []byte) int {
		n, addr, err := s.conn.ReadFromUDP(buf)
        if err != nil {
            fmt.Println("Error: ", err)
        }

        fmt.Println(n, "bytes Received from", addr)

		s.addr = addr;
		return n
}

func ReadConfig(filename string) *Config {

	var config Config

	data, err := ioutil.ReadFile(filename)
	handleErr(err)

	err = yaml.Unmarshal(data, &config)
	handleErr(err)

	return &config
}

func handleErr(err error) {
	if nil != err {
		log.Fatal(err)
	}
}

func InitConns(conf *Config) (*Socket, *Socket) {
	PROTO := "udp"

	saddr, err := net.ResolveUDPAddr(PROTO, conf.Listen)
	handleErr(err)

	sconn, err := net.ListenUDP(PROTO, saddr)
	handleErr(err)

	taddr, err := net.ResolveUDPAddr(PROTO, conf.Target)
	handleErr(err)

    tconn, err := net.DialUDP(PROTO, nil, taddr) // connection-based
    handleErr(err)

	return &Socket{sconn, nil, true}, &Socket{tconn, nil, false}
}

func main() {
	// 0. read configurations
	config := ReadConfig("./config.yaml")

	// damonize??

	// 1. initiate client and target
	client, target := InitConns(config)
	defer func() {
		client.conn.Close() 
		target.conn.Close()
	}()
	
	cache := make(Cache)

	// 2. wait for connections, read, and distribute
	buf := make([]byte, 1024)
 
    for {
		// read from client
        len := client.recv(buf)

		// check Cache
		i := 12
		for  ; buf[i] != 0x00 ;i++ {
		}
		qStr := string(buf[12:i])
		fmt.Println("Query:",qStr);

		cbuf, ok := cache[qStr]
		if ok { // send cached buf back to client
			
			// id copy
			cbuf[0] = buf[0]
			cbuf[1] = buf[1]
			_ = client.send(cbuf)

			fmt.Println("From cache");

		}else{

			// write to target
			_ = target.send(buf[:len])

			// read response from target
        	len = target.recv(buf)

			// write response to client
			_ = client.send(buf[:len])
	
			cache[qStr] = make([]byte, len)
			copy(cache[qStr], buf)
		}
    }
	fmt.Println("done")
}
