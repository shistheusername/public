package main

import "testing"

func TestReadConfig(t *testing.T) {
	config := ReadConfig("./config.yaml")
	if "" == config.Listen {
		t.Errorf("ReadConfig failed: Listen, expected non empty string, actual: %v", config.Listen)
	}
}

func TestInitiateSrv(t *testing.T) {
	config := ReadConfig("./config.yaml")
	conn := InitiateSrv(config)
	defer conn.Close()
	if conn == nil {
		t.Errorf("InitialteSrv failed")
	}
}
