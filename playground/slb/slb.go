package main

import (
	"os"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

type msgBackend struct {
	url *url.URL
	msg int
}

type loadBalancer struct {
	ch2scheduler chan msgBackend
}

const (
    GET_BACKEND = iota
    OK_BACKEND
    ERR_BACKEND
)

var (
	// TODO: read from a configuration file
	rewriteRules = map[string]string{ 
					"/api/": "/",
					"/api/v2/": "/v2/",
					"/api/v1/": "/v1/",
					}
)

func (lb *loadBalancer) ServeHTTP(rw http.ResponseWriter, r *http.Request) {

	// have a cache of proxies??

	// fetch a backend
	lb.ch2scheduler <- msgBackend{ nil, GET_BACKEND }
	backend := <-lb.ch2scheduler

	if backend.msg != OK_BACKEND {
		log.Printf("ERROR: No available backend")
		rw.WriteHeader(http.StatusBadGateway)

		return
	}

	applyRewriteRules(r.URL)

	proxy := httputil.NewSingleHostReverseProxy(backend.url)
	proxy.ServeHTTP(rw, r)

	return
}

func applyRewriteRules(reqUrl *url.URL) {
	
	targetPath := rewriteRules[reqUrl.Path]
	if targetPath != "" {
		log.Printf("UrlRewrite: %s -> %s", reqUrl.Path, targetPath)
		reqUrl.Path = targetPath
	}
}

func main() {
	
	ch := make(chan msgBackend)
	lb := loadBalancer { ch }
	port := "2019"

	// start scheduler
	go Scheduler(ch)
	http.Handle("/", &lb)

	if len(os.Args) == 2 {
		port = os.Args[1]
	}

	log.Printf("Running on %s", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
