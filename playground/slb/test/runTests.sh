#!/bin/sh

. ./functions

for P in 1234 1235 2019
do
	echo "========= PORT $P =========="
	sendRequests $P
	echo "=============================="
	echo ""
done
