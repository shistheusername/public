#!/bin/sh
[ ! -d node_modules ] && npm install

for PORT in 1234 1235
do
	node index.js $PORT &
done
