const express = require('express')
const app = express()
const port = process.argv[2] || 3000
app.use(express.json())

app.get('/', (req, res) => { 
		res.send(`Hello World on ${port}!`)
})

app.get('/v1/', (req, res) => { 
		res.send(`This is v1 on ${port}, Query string: ${req.originalUrl}`)
})

app.get('/v2/', (req, res) => { 
		res.send(`This is v2 on ${port}, Query string: ${req.originalUrl}`)
})

app.post('/', (req, res) => {
  		res.send(req.body)
})

app.listen(port, () => console.log(`The most useful app listening on port ${port}!`))
