package main

import (
//	"fmt"
	"errors"
//	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

var ups map[string]*upStream

type upStream struct {
	url *url.URL
	isUp bool
}

type hcResultMsg struct {
	backend string
	status bool
}

func Scheduler(ch2lb chan msgBackend) {

	// TODO: read from a configuration file
	backends := []string{"http://localhost:1234", "http://localhost:1235"}

	hcmsg, err := initUpStreams(backends)
	if err != nil {
		log.Fatal(err)
	}

	for {
		select {
		case <-ch2lb:
			backend, err := chooseBackend()
			if err != nil {
				ch2lb <- msgBackend { nil, ERR_BACKEND }
			}else{
				ch2lb <- msgBackend { backend.url, OK_BACKEND }
			}
		case hc := <-hcmsg:
			ups[hc.backend].isUp = hc.status
			log.Println(hc)
		}
	}
}

func chooseBackend() (*upStream, error) {

	// should be a random order
	for _, target := range ups {
		if target.isUp {
			return target, nil
		}
	}

	// TODO: add more LB algorithms
	return nil, errors.New("No upstreams available")
}

func initUpStreams(backends []string) (chan hcResultMsg, error) {
	ups = make(map[string]*upStream)
	ch := make(chan hcResultMsg)

	for _, target := range backends {

		// populate upstream map
		burl, err := url.ParseRequestURI(target)
		if err != nil {
			log.Println(err)
			continue
		}

		ups[target] = &upStream { burl, false }

		// start health check
		go healthCheck(target, ch)
	}

	if len(ups) == 0 {
		return nil, errors.New("Failed to initiate upstreams")
	}

	return ch, nil
}

func healthCheck(target string, ch chan <- hcResultMsg) {
	for {
		_, err := http.Get(target)
		if err != nil {
			ch <- hcResultMsg{ target, false }
			log.Println(err)
		}else{
			ch <- hcResultMsg{ target, true }
			// body, _ := ioutil.ReadAll(res.Body)
			// res.Body.Close()
			// fmt.Println(string(body))
		}
		time.Sleep(5 * time.Second)
	}
}
