package main

import (
	"bufio"
	"bytes"
	"container/heap"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const MaxInt = 1<<32 - 1

type Graph struct {
	src, dst *Vertex
	pq       PriorityQueue
	vertices map[string]*Vertex
}

type Edges map[string]int

type Vertex struct {
	name       string
	dist       int
	neighbours Edges
	index      int
	prev       *Vertex
}

func (g *Graph) String() string {
	var buffer bytes.Buffer
	for _, v := range g.vertices {
		buffer.WriteString(fmt.Sprintf("%v\n", v))
	}
	return buffer.String()
}

func (v *Vertex) String() string {
	return fmt.Sprintf("%v(%v)", v.name, v.dist)
}

func OpenDataFile(filename string) (*os.File, *bufio.Scanner) {
	file, err := os.Open(filename)
	if nil != err {
		log.Fatal(err)
	}
	return file, bufio.NewScanner(file)
}

func (g *Graph) MakeVertex(a, peer string, dist int) *Vertex {

	v, ok := g.vertices[a]
	if !ok { // new
		if dist == 0 { //src
			v = &Vertex{name: a, dist: 0, neighbours: make(Edges)}
		} else {
			v = &Vertex{name: a, dist: MaxInt, neighbours: make(Edges)}
		}
		(&(g.pq)).Push(v)
		g.vertices[a] = v
	}

	if "" != peer {
		v.neighbours[peer] = dist
	}

	return v
}

func ParseLine(line string, g *Graph) {
	tokens := strings.Split(line, " ")
	switch len(tokens) {
	case 2: // start line: src dst
		g.src = g.MakeVertex(tokens[0], "", 0)
		g.dst = g.MakeVertex(tokens[1], "", MaxInt)
	case 3: // edge line: v u dist
		dist, _ := strconv.Atoi(tokens[2])
		g.MakeVertex(tokens[0], tokens[1], dist)
		g.MakeVertex(tokens[1], tokens[0], dist)
	default: // invalid lines, just ignore
	}
}

func InitiateGraph(filename string) Graph {
	file, scanner := OpenDataFile(filename)
	defer file.Close()

	g := Graph{pq: PriorityQueue{}, vertices: map[string]*Vertex{}}
	for scanner.Scan() {
		ParseLine(scanner.Text(), &g)
	}
	return g
}

func (g *Graph) ShortestPath() {
	heap.Init(&(g.pq))
	for g.pq.Len() > 0 {
		v := heap.Pop(&(g.pq)).(*Vertex)
		for n, dist := range v.neighbours {
			u := g.vertices[n]
			alt := v.dist + dist
			if alt < u.dist {
				(&(g.pq)).Update(u, alt)
				u.prev = v
			}
		}
	}
}

func (g *Graph) printOut() {
	var result string
	d := g.vertices[g.dst.name]
	for d.prev != nil {
		result = fmt.Sprintf("%v %v", d, result)
		d = d.prev
	}
	fmt.Println(fmt.Sprintf("%v %v", g.src, result))
}

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Usage: %v datafile\n", os.Args[0])
	}

	// 1. create a Graph from data file
	g := InitiateGraph(os.Args[1])

	// 2. run dijikstra
	g.ShortestPath()

	// 3. print the result
	g.printOut()
}
