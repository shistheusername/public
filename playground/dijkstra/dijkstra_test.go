package main

import "testing"

func TestMakeVertex(t *testing.T) {
	g := Graph{pq: PriorityQueue{}, vertices: map[string]*Vertex{}}
	g.MakeVertex("S", "D", 10)

	if len(g.vertices) != 1 {
		t.Errorf("MakeVertex failed - expect:%v, actual:%v", 1, len(g.vertices))
	}

	v := g.vertices["S"]
	if len(v.neighbours) != 1 {
		t.Errorf("MakeVertex neghbours failed - expect:%v, actual:%v", 1, len(v.neighbours))
	}

	g.MakeVertex("R", "", 0)
	if g.vertices["R"].dist != 0 {
		t.Errorf("MakeVertex src failed - expect:%v, actual:%v", 0, g.vertices["R"].dist)
	}
}

func TestOpenDataFile(t *testing.T) {
	file, scanner := OpenDataFile("./test.txt")
	defer file.Close()

	if file == nil && scanner == nil {
		t.Errorf("OpenFile failed")
	}
}

func TestInitiateGraph(t *testing.T) {
	g := InitiateGraph("./test.txt")
	if len(g.pq) != 8 {
		t.Errorf("Graph initiation failed, expected: %v, actual: %v", 8, len(g.pq))
	}

	if g.src.name != "H" {
		t.Errorf("Graph src initiation failed, expected: %v, actual: %v", "H", g.src.name)
	}

	if g.dst.name != "S" {
		t.Errorf("Graph dst initiation failed, expected: %v, actual: %v", "S", g.dst.name)
	}
}

func TestShortestPath(t *testing.T) {
	g := InitiateGraph("./test.txt")
	g.ShortestPath()

	if len(g.pq) != 0 {
		t.Errorf("ShortestPath failed, expected: %v, actual: %v", 0, len(g.pq))
	}
}
