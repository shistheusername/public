// code from: https://golang.org/pkg/container/heap/
package main

import (
	"container/heap"
)

type PriorityQueue []*Vertex

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].dist < pq[j].dist
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Vertex)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old) - 1
	item := old[n]
	item.index = -1 // for safety
	*pq = old[0:n]
	return item
}

// update modifies the dist and value of a Vertex in the queue.
func (pq *PriorityQueue) Update(item *Vertex, dist int) {
	item.dist = dist
	heap.Fix(pq, item.index)
}
